#!/usr/bin/perl -w -I/opt/rtcds/rtscore/trunk/src/perldaq -I/ligo/cdscfg 

# This script claculates an average of $mychan starting 5 seconds ago up till now

use threads;
use threads::shared;
BEGIN {
     $epicsbase = $ENV{'EPICS_BASE'};
         $epicslib = $epicsbase . "/lib/linux-x86_64";
         $epicsperl = $epicsbase . "/lib/perl";
        $rcgdir = $ENV{'RCG_DIR'};
        $rcgdir .= "/test";
         print "EPICSBASE = $epicsbase \n";
         print "EPICSLIB = $epicslib \n";
         print "EPICSPERL = $epicsperl \n";
         push @INC,"/opt/cdscfg/tst/stddir.pl";
         push @INC,"$epicsperl";
         push @INC,"$epicslib";
        push @INC,"/opt/rtapps/debian10/perlmodules";
        push @INC,"/opt/rtcds/rtscore/advligorts/test/rcgtest/perl";
        push @INC,"/usr/lib/x86_64-linux-gnu/perl5/5.28/auto";
        push @INC,"/usr/lib/x86_64-linux-gnu/perl5/5.28";
        push @INC,"/opt/rtcds/rtscore/advligorts/src/perldaq";

}
#use stdenv;
#INIT_ENV(1);
use CaTools;
use Time::HiRes qw( usleep );
use POSIX qw(ceil floor);
use File::Path;
use DtTools;

$rcgtestdir = $ARGV[0];

$rcgtestdatadir = $rcgtestdir . "/data";
print "Creating directory $rcgtestdatadir \n";
$rcgtestimagesdir = $rcgtestdir . "/images";
$rcgtesttmpdir = $rcgtestdir . "/tmp";


# Need daqperl module to get data from NDS
require "daq.pm";

sub testReq
{
	my $pf = "PASS";
	my $low = shift;
	my $hi = shift;
	my $val = shift;

	if(($val >= $hi) || ($val <= $low)) {
		$pf = "FAIL";
	}
	return($pf);
}

#set test requirements
my $dtMinAmp = 1300;
my $dtMaxAmp = 1360;
my $dtMinFreq = 959;
my $dtMaxFreq = 962;
my $dtAvgReq = 1;

$status = 0;
caPut("X2:IOP-SAM_TEST_STAT_2",2);


# Test 1: Verify frequency is correct by using DTT to do an FFT **************************************
# Use DTT Diag tool to get FFT on DT channel
#dtTestStart("./dtrunfile.sh","./dtrunfile.cmd","duotoneTest.xml","dtdata.xml");
$runfile = $rcgdir . "/rcgtest/perl/duotone/dtrunfile.sh";
$cmdfile = $rcgtesttmpdir . "/dtrunfile.cmd";
$templatefile = $rcgdir . "/rcgtest/perl/duotone/duotoneTest.xml";
$results_file = $rcgtesttmpdir . "/dtdata.xml";
$ref_data_file = $rcgtesttmpdir . "/dtreference.data";
$test_data_file = $rcgtesttmpdir . "/dt.data";

#Start Diag session based on template.
dtTestStart($runfile,$cmdfile,$templatefile,$results_file);

#system("./dtrunfile.sh");
#(my $testPts,my $testTotal,my @testData) = dtReadData("dtdata.xml","dt.data","DATA");
(my $testPts,my $testTotal,my @testData) = dtReadData($results_file,$test_data_file,"DATA");
#Calc Data Average
my $testAvg = $testTotal / $testPts;
print "Avg value = $testAvg \n";

# Get template data and frequencies
#($testPts,my $refTotal,my @freq) = dtReadData("duotoneTest.xml","dtreference.data","FREQ");
#($testPts,$refTotal,my @refData) = dtReadData("duotoneTest.xml","dtreference.data","DATA");
($testPts,my $refTotal,my @freq) = dtReadData($templatefile,$ref_data_file,"FREQ");
($testPts,$refTotal,my @refData) = dtReadData($templatefile,$ref_data_file,,"DATA");

# Produce FFT plot
system('/usr/bin/gnuplot /opt/rtcds/rtscore/advligorts/rcgtest/perl/duotone/duotoneTest.plt');
#Calc Reference Average
my $refAvg = $refTotal / $testPts;

# Get peak signal amp and corresponding frequency
(my $testPeakAmp,my $testPeakFreq) = dtFindPeak($testPts,@freq,@testData);
(my $refPeakAmp,my $refPeakFreq) = dtFindPeak($testPts,@freq,@refData);

print "Peak = $testPeakAmp at $testPeakFreq Hz \n";
print "Peak = $refPeakAmp at $refPeakFreq Hz \n\n\n";

# Test against requirements
my $dtAmpPf = testReq($dtMinAmp,$dtMaxAmp,$testPeakAmp);
my $dtFreqPf = testReq($dtMinFreq,$dtMaxFreq,$testPeakFreq);
my $avgMaxReq = $refAvg + $dtAvgReq;
my $avgMinReq = $refAvg - $dtAvgReq;
my $dtAvgPf = testReq($avgMinReq,$avgMaxReq,$testAvg);

print "Amp Test = $dtAmpPf \n";
print "Freq Test = $dtFreqPf \n";
print "Avg Test = $dtAvgPf \n";

if(($dtAmpPf eq "FAIL") || ($dtFreqPf eq "FAIL") || ($dtAvgPf eq "FAIL")) {$status = -1;}

#Get DT channel data from DAQ system ***************************************************************
$adcDT = "X2:IOP-SAM_ADC_DUOTONE_OUT_DQ";

my @epicsChans = qw( X2:FEC-110_DUOTONE_TIME  X2:FEC-110_IRIGB_TIME X2:FEC-110_DUOTONE_TIME_DAC);

print "DAQ connect \n";
# get the channel list
DAQ::connect("localhost", 8088);

# get current time
$gps = DAQ::gps();

# want to get data in past from frames
my $gpss = int($gps);
$gpss -= 60;
print "gps time is $gps \n";

# Get data from NDS for 3 second period, starting on 1 second boundary
@adcDTdata = DAQ::acquire($adcDT, 3, $gpss);

# Write data to file for later plotting as part of test report
open(OUTNDS,">/tmp/rcgtest/tmp/duotoneNds.data") || die "cannot open test data file for writing";
for($ii=0;$ii<200;$ii++) {
	print OUTNDS "$ii	$adcDTdata[$ii] \n";
}
close(OUTNDS);

my $jj = 0;
my $kk = 0;

# Test 2: Find minima in 1 second of duotone data ****************************************
# Perform Window averaging to find data minima location
my $winAvg;
my $winTotal = 0;
my $winMin = 50000;
my $winMinPt = 0;
$winAvgAmpReqLo = 1.0;
$winAvgAmpReqHi = 3.0;
$winAvgCycReqLo = 32760;
$winAvgCycReqHi = 32770;

for($ii=0;$ii<65472;$ii++)
{
	$winTotal = 0;
	for($kk=0;$kk<64; $kk++)
	{
		$jj = $ii + $kk;
		$winTotal += abs($adcDTdata[$jj]);
	}
	$winAvg = $winTotal / 64;
	if($winAvg < $winMin)
	{
		$winMin = $winAvg;
		$winMinPt = $ii;
	}
}

$winMinPt += 32;
my $winAvgPf = testReq($winAvgAmpReqLo,$winAvgAmpReqHi,$winMin);
my $winMinPf = testReq($winAvgCycReqLo,$winAvgCycReqHi,$winMinPt);
if(($winAvgPf eq "FAIL") || ($winMinPf eq "FAIL")) {$status = -1;}
print "Window min = $winMin at $winMinPt \n";
	
$kk = 0;

# Get total to calc mean value; find max and min points in data
$dtTotal = 0;
$minVal = 100000.0 + 1.00;
$minPt = 0;
$max = -1000000.0 - 1.0;
$maxPt = 0;

for($ii=0;$ii<65536;$ii++)
{
	#print "DT = $adcDTdata[$ii]\n";
	$dtTotal += $adcDTdata[$ii];
	if($adcDTdata[$ii] < $minVal) 
	{ 
		$minVal = $adcDTdata[$ii]; 
		$minPt = $ii;
	}
	if($adcDTdata[$ii] > $max) 
	{ 
		$max = $adcDTdata[$ii]; 
		$maxPt = $ii;
	}
}

$meanVal = $dtTotal/65536;
# ***************************************
# Perform linear fit to determine time of zero crossing

$x = 0.0;
$sumX = 0.0;
$sumY = 0.0;
$sumXX = 0.0;
$sumXY= 0.0;
$xInc = 1000000.0/65536.0;
$startCnt = 0;
$count = 12;

  for($ii=$startCnt;$ii<$count;$ii++)
  {
          $y = $adcDTdata[$ii];
          $sumX += $x;
          $sumY += $y;
          $sumXX += $x * $x;
          $sumXY += $x * $y;
          $x += $xInc;
  }
  $msumX = $sumX * -1;
  $den = ($count*$sumXX-$sumX*$sumX);
  if($den != 0.0)
  {
  $offset = ($msumX*$sumXY+$sumXX*$sumY)/$den;
  $slope = ($msumX*$sumY+$count*$sumXY)/$den;
  }
  $meanVal -= $offset;
  if($slope != 0.0)
  {
  $answer = $meanVal/$slope;
  }


$dtTestResult = "PASS";
$asvTestResult = "PASS";
$fedtTestResult = "PASS";
$fedtdTestResult = "PASS";
$feibTestResult = "PASS";

#Get DT and IRIG-B timing info from EPICS
@epicsVals = caGet(@epicsChans);

if(($answer > 4) || ($answer < 2)) { $dtTestResult = "FAIL"; $status = -1; }
if(($meanVal > 80) || ($meanVal < 60)) { $asvTestResult = "FAIL"; $status = -1; }
if(($epicsVals[0] > 6) || ($epicsVals[0] < 4)) { $fedtTestResult = "FAIL"; $status = -1; }
if(($epicsVals[1] > 15) || ($epicsVals[1] < 4)) { $feibTestResult = "FAIL"; $status = -1; }
if(($epicsVals[2] >= 72) || ($epicsVals[2] <= 68)) { $fedtdTestResult = "FAIL"; $status = -1; }

if($status == 0) {
    $test_summary =  "PASS";
} else {
    $test_summary =  "FAIL";
}


$dtMeas = int $answer;
$dtMean = int $meanVal;



# ***************************************

(my $sec,my $min, my $hour, my $day, my $mon, my $year, my $wday, my $yday, my $isdst) = localtime(time);
$year += 1900;
my @abbr = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );

my @strEpics = ("X2:FEC-110_RCG_VERSION","X2:FEC-110_MSG","X2:FEC-110_MSGDAQ","X2:FEC-110_RCG_VERSION");
my @strEpicsVals;
@strEpicsVals = caGet(@strEpics);

my $tPeakFreq = sprintf("%.1f",$testPeakFreq);
my $amr = sprintf("%.1f",$avgMinReq);
my $amxr = sprintf("%.1f",$avgMaxReq);
my $tavg = sprintf("%.1f",$testAvg);
my $twinMin = sprintf("%.2f",$winMin);
open(OUTM,">/tmp/rcgtest/data/duotoneTestData.dox") || die "cannot open test data file for writing";
print OUTM <<END;
/*!     \\page duotoneTest Duotone Test Results
*       \\verbatim
*************************************************************************************************

ADC DUOTONE TIMING SIGNAL  TEST REPORT 

TIME: $hour:$min:$sec  $abbr[$mon]  $day $year

RCG version number: $strEpicsVals[3]

*************************************************************************************************

PURPOSE: Purpose of this test is to verify code timing by checking the duotone signal injected
into the first ADC, last channel of every I/O chassis.

OVERVIEW: This test performs an FFT, and several other timing measurements, using the duotone
signal injected into an I/O chassis ADC module. 

TEST REQUIREMENTS:
	This test is designed to run on the LHO DAQ test system, and requires:
		- DAQ system operational.
		- DTT diag tool available.
		- Model x1iopsam running on Caltech scipe20 computer.
		- Test script: /opt/rtcds/userapps/trunk/cds/test/scripts/dt/duotoneTest.pl
		- DTT files: 
			- /opt/rtcds/userapps/trunk/cds/test/scripts/dt/dtrunfile.sh
			- /opt/rtcds/userapps/trunk/cds/test/scripts/dt/duotoneTest.xml

TEST PROCEDURE:
1) Perform FFT to verify correct DT frequency.
2) Perform line fit to determine zero crossing at 1Hz boundary.
3) Use window averaging method to determine minima of the signal, which should occur at the
   half second point.
4) Read EPICS timing diagnostic information from the running code.
5) Plot the FFT data.
6) Plot the time series data for 200 samples after 1Hz mark to verify proper signal phase.

       \\endverbatim

*       \\verbatim
TEST RESULTS:


-------------------------------------------------------------------------------------------------
      TEST           	           REQUIREMENT        	MEASURED		PASS/FAIL
-------------------------------------------------------------------------------------------------
FFT Results
	- Frequency (Hz)	$dtMinFreq to $dtMaxFreq		$tPeakFreq			$dtFreqPf
	- Peak Amplitude	$dtMinAmp to $dtMaxAmp		$testPeakAmp		$dtAmpPf
	- Avg Amplitude		$amr to $amxr		$tavg			$dtAvgPf

Calculated from DT Signal

	- DT Time		  3 +/-1		$dtMeas			$dtTestResult
	- Average Signal Value	  70 +/- 10		$dtMean 			$asvTestResult
	- Window Avg		
		- Amp 		$winAvgAmpReqLo to $winAvgAmpReqHi			$twinMin			$winAvgPf
		- Cycle		$winAvgCycReqLo to $winAvgCycReqHi		$winMinPt			$winMinPf

	- Additional Reference Data
		- Max Value				$max at $maxPt
		- Min Value				$minVal at $minPt
		- T-2 Value				$adcDTdata[65534]
		- T-1 Value				$adcDTdata[65535]
		- T0 Value				$adcDTdata[0]
		- T1 Value				$adcDTdata[1]

Diagnostics from FE Code

	- ADC DT Time		5 +/- 2			$epicsVals[0]			$fedtTestResult
	- DAC DT Time		70 +/- 2		$epicsVals[2]			$fedtdTestResult
	- IRIG-B Time		13+/-2 			$epicsVals[1]			$feibTestResult


TEST SUMMARY:  $test_summary

       \\endverbatim
*       \\verbatim

       \\endverbatim
\\image html duotone.png 
\\image latex duotone.png 
*       \\verbatim

       \\endverbatim
\\image html duotoneNds.png 
\\image latex duotoneNds.png 

*\/

END

close OUTM;
system("cat /tmp/rcgtest/data/duotoneTestData.dox");
caPut("X2:IOP-SAM_TEST_STAT_2",1);
if($status < 0) {
    caPut("X2:IOP-SAM_TEST_STAT_2",0);
}

exit($status);


