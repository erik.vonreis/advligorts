#!/usr/bin/perl -w -I/opt/rtcds/rtscore/trunk/src/perldaq

# Script for testing IPC components

use threads;
use threads::shared;
BEGIN {
     $epicsbase = $ENV{'EPICS_BASE'};
         $epicslib = $epicsbase . "/lib/linux-x86_64";
         $epicsperl = $epicsbase . "/lib/perl";
         $rcgdir = $ENV{'RCG_DIR'};
         print "EPICSBASE = $epicsbase \n";
         print "EPICSLIB = $epicslib \n";
         print "EPICSPERL = $epicsperl \n";
         push @INC,"/opt/cdscfg/tst/stddir.pl";
         push @INC,"$epicsperl";
         push @INC,"$epicslib";
	 push @INC,"/opt/rtapps/debian10/perlmodules";
        push @INC,"/opt/rtcds/rtscore/advligorts/test/rcgtest/perl";
        push @INC,"/usr/lib/x86_64-linux-gnu/perl5/5.28/auto";
        push @INC,"/usr/lib/x86_64-linux-gnu/perl5/5.28";
        push @INC,"/opt/rtcds/rtscore/advligorts/src/perldaq";
}
#use stdenv;
use CaTools;
use Time::HiRes qw( usleep );
use POSIX qw(ceil floor);
use File::Path;

sub testStart
{
	my $runFile = shift;
	my $diagFile = shift;
	my $dataFile = shift;
	
	open DFILE,">","/tmp/rcgtest/tmp/oscRunFile.cmd" or die $!;
	print DFILE "restore $diagFile \n";
	print DFILE "run -w \n";
	print DFILE "save $dataFile \n";
	print DFILE "quit \n";
	close(DATAFILE);

	system ($runFile);
}
sub readData
{
	my $xmlFile = shift;
	my $dataFile = shift;
	my $request = shift;

	my $convCall = "/usr/bin/xmlconv -m ";
	$convCall .= $xmlFile;
	$convCall .= " ";
	$convCall .= $dataFile;
	$convCall .= " \"Result[0]\"";
	system ($convCall);

	my $total = 0;
	my @data;
	my @vals;
	my $ii = 0;

    printf "file = $dataFile \n"; 
	open DATAFILE,"<","$dataFile" or die $!;
	while(<DATAFILE>) {
		my @vals = split(" ",$_);
		if($request eq "DATA") {
			$data[$ii] = $vals[1];
		}else{
			$data[$ii] = $vals[0];
		}
		$total += $data[$ii];
		$ii ++;
	}
	close(DATAFILE);
	return($ii,$total,@data);
}
sub findPeak
{
	my $pts = shift;
	my @data = @_;
	my $ii = 0;
	
	my $PeakAmp = -100000000;
	my $PeakFreq = 0;
	my $jj = $pts;

	for($ii=0;$ii<$pts;$ii++) {
		if($data[$jj] > $PeakAmp) {
			$PeakAmp = $data[$jj];
			$PeakFreq = $data[$ii];
		}
		$jj ++;
	}
	return($PeakAmp,$PeakFreq);
}
sub checkData
{
	my $pts = shift;
	my $spec = shift;
	my @data = @_;
	my $ii = 0;
	my $jj = $pts;
	my @diff;
	my $errCnt = 0;

	for($ii=0;$ii<$pts;$ii++) {
		$diff[$ii] = abs($data[$ii] - $data[$jj]);
		if($diff[$ii] > $spec) {
			printf "Value $ii is out of spec - $data[$ii]  $data[$jj] $diff[$ii]\n";
			$errCnt ++;
		}
		$jj ++;
	}
	return($errCnt,@diff);
}

my $ii = 0;
my $refPeakAmp = -100000000;
my $refPeakFreq = 0;
my $testPeakAmp = -100000000;
my $testPeakFreq = 0;
my $status = 0;
my @reqVals = (0.0001, 0.0001, 0.00005, 0.00001, 0.00001);
my @testRes = qw(PASS PASS PASS PASS PASS PASS PASS);
my @tAvg;
my @tAvgDiff;
my @tErr;
my @tFreq;
my @tAmp;
my @rAvg;


(my $sec,my $min, my $hour, my $day, my $mon, my $year, my $wday, my $yday, my $isdst) = localtime(time);
$year += 1900;
my @abbr = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
my @strEpics = ("X2:FEC-112_RCG_VERSION","X2:FEC-112_MSG","X2:FEC-112_MSGDAQ","X2:FEC-112_RCG_VERSION");
my @strEpicsVals;
@strEpicsVals = caGet(@strEpics);

caPut("X2:IOP-SAM_TEST_STAT_9",2);

print  "\n\nSTARTING OSCILLATOR MODULE TESTING *********************************************\n";
print  "TIME: $hour:$min:$sec  $abbr[$mon]  $day $year \n";
print "RCG Version: $strEpicsVals[3]\n\n";
print "SVN Version: $strEpicsVals[0]\n\n";


caPut("X2:IOP-SAM_TEST_STATUS","OSC TEST - 2000Hz");


# Setup Oscillators on senders
my @epicsChans = qw( X2:ATS-TIM16_T1_OSC1_CLKGAIN  X2:ATS-TIM16_T1_OSC1_FREQ );
my @epicsVals = (2000,2000);
caPut(@epicsChans,@epicsVals);
sleep(2);
$rcgtestdir = $ARGV[0];

$rcgtesttmpdir = $rcgtestdir . "/tmp";
$rcgtestdatadir = $rcgtestdir . "/data";
$rcgtestimagesdir = $rcgtestdir . "/images";
$runfile = $rcgdir . "/test/rcgtest/perl/osc/oscrunfile.sh";


#Start 2KHz OSC Test *****************************************************************************
$templatefile = $rcgdir . "/test/rcgtest/perl/osc/osctest2000.xml";
$results_file = $rcgtesttmpdir . "/osc2000data.xml";
testStart($runfile,$templatefile,$results_file);
#Get 2KHz OSC Test Data
$test_data_file = $rcgtesttmpdir . "/osc2000.data";
(my $testPts,my $testTotal,my @testData) = readData($results_file,$test_data_file,"DATA");
#Calc avg value of all points.
my $testAvg = $testTotal / $testPts;

# Get Test Reference Frequencies
$test_data_file = $rcgtesttmpdir . "/osc2000reference.data";
($testPts,my $refTotal,my @freq) = readData($templatefile,$test_data_file,"FREQ");
# Get Test Reference Data
($testPts,$refTotal,my @refData) = readData($templatefile,$test_data_file,"DATA");
#Calc avg value of all points.
my $refAvg = $refTotal / $testPts;

#Check test data against reference data
(my $errs,@diff) = checkData($testPts,$reqVals[0],@refData,@testData);
print "Checked 2K Data - $errs errors  $diff[0] \n";
if($errs) {
	$testRes[0] = "FAIL - PT ERRORS";
	$status = -1;
}


#Find Peak amp and frequencies for reference and test data
($refPeakAmp,$refPeakFreq) = findPeak($testPts,@freq,@refData);
($testPeakAmp,$testPeakFreq) = findPeak($testPts,@freq,@testData);

my $avgDiff = abs($refAvg - $testAvg);
print "Reference - Avg = $refAvg with Peak Amp = $refPeakAmp at $refPeakFreq Hz \n";
print "Test - Avg - $testAvg with Peak Amp = $testPeakAmp at $testPeakFreq Hz \n";
print "Avg diff = $avgDiff \n";

$tAvg[0] = sprintf("%.3f",$testAvg);
$tErr[0] = $errs;
$tFreq[0] = sprintf("%.3f",$testPeakFreq);
$tAmp[0] = sprintf("%.3f",$testPeakAmp);
$tAvgDiff[0] = sprintf("%.4e",$avgDiff);
#End of 2k OSC TEST

#Start 10Hz OSC TEST ***********************************************************************************
# Setup Oscillators on senders
@epicsVals = (2000,10);
caPut(@epicsChans,@epicsVals);
sleep(2);
$templatefile = $rcgdir . "/test/rcgtest/perl/osc/osctest10.xml";
$results_file = $rcgtesttmpdir . "/osc10data.xml";
testStart($runfile,$templatefile,$results_file);
#Get 2KHz OSC Test Data
$test_data_file = $rcgtesttmpdir . "/osc10.data";

#Get 2KHz OSC Test Data
($testPts,$testTotal,@testData) = readData($results_file,$test_data_file,"DATA");
#Calc avg value of all points.
$testAvg = $testTotal / $testPts;

# Get Test Reference Frequencies
$test_data_file = $rcgtesttmpdir . "/osc10reference.data";
($testPts, $refTotal, @freq) = readData($templatefile,$test_data_file,"FREQ");
# Get Test Reference Data
($testPts,$refTotal, @refData) = readData($templatefile,$test_data_file,"DATA");
#Calc avg value of all points.
$refAvg = $refTotal / $testPts;

#Check test data against reference data
($errs,@diff) = checkData($testPts,$reqVals[1],@refData,@testData);
print "Checked 10Hz Data - $errs errors  $diff[0] \n";
if($errs) {
	$testRes[1] = "FAIL";
	$status = -1;
}


#Find Peak amp and frequencies for reference and test data
($refPeakAmp,$refPeakFreq) = findPeak($testPts,@freq,@refData);
($testPeakAmp,$testPeakFreq) = findPeak($testPts,@freq,@testData);

$avgDiff = abs($refAvg - $testAvg);
print "Reference - Avg = $refAvg with Peak Amp = $refPeakAmp at $refPeakFreq Hz \n";
print "Test - Avg - $testAvg with Peak Amp = $refPeakAmp at $refPeakFreq Hz \n";
print "Avg diff = $avgDiff \n";

$tAvg[1] = sprintf("%.3f",$testAvg);
$tErr[1] = $errs;
$tFreq[1] = sprintf("%.3f",$testPeakFreq);
$tAmp[1] = sprintf("%.3f",$testPeakAmp);
$tAvgDiff[1] = sprintf("%.4e",$avgDiff);
#End of 10Hz OSC TEST

#Start 6500Hz OSC TEST ***********************************************************************************
# Setup Oscillators on senders
@epicsVals = (2000,6500);
caPut(@epicsChans,@epicsVals);
sleep(2);
$templatefile = $rcgdir . "/test/rcgtest/perl/osc/osctest6500.xml";
$results_file = $rcgtesttmpdir . "/osc6500data.xml";
testStart($runfile,$templatefile,$results_file);
#Get 2KHz OSC Test Data
$test_data_file = $rcgtesttmpdir . "/osc6500.data";

#Get 6.5KHz OSC Test Data
($testPts,$testTotal,@testData) = readData($results_file,$test_data_file,"DATA");
#Calc avg value of all points.
$testAvg = $testTotal / $testPts;

# Get Test Reference Frequencies
$test_data_file = $rcgtesttmpdir . "/osc6500reference.data";
($testPts, $refTotal, @freq) = readData($templatefile,$test_data_file,"FREQ");
# Get Test Reference Data
($testPts,$refTotal, @refData) = readData($templatefile,$test_data_file,"DATA");
#Calc avg value of all points.
$refAvg = $refTotal / $testPts;

#Check test data against reference data
($errs,@diff) = checkData($testPts,$reqVals[2],@refData,@testData);
print "Checked 6500Hz Data - $errs errors  $diff[0] \n";


#Find Peak amp and frequencies for reference and test data
($refPeakAmp,$refPeakFreq) = findPeak($testPts,@freq,@refData);
($testPeakAmp,$testPeakFreq) = findPeak($testPts,@freq,@testData);

$avgDiff = abs($refAvg - $testAvg);
print "Reference - Avg = $refAvg with Peak Amp = $refPeakAmp at $refPeakFreq Hz \n";
print "Test - Avg - $testAvg with Peak Amp = $testPeakAmp at $testPeakFreq Hz \n";
print "Avg diff = $avgDiff \n";

$tAvg[2] = sprintf("%.3f",$testAvg);
$tErr[2] = $errs;
$tFreq[2] = sprintf("%.3f",$testPeakFreq);
$tAmp[2] = sprintf("%.3f",$testPeakAmp);
$tAvgDiff[2] = sprintf("%.4e",$avgDiff);
if($errs || ($tFreq[2] != 6500)) {
	$testRes[2] = "FAIL";
	$status = -1;
}
#End of 6500Hz OSC TEST

if($status == 0) {
    $test_summary =  "PASS"
} else {
    $test_summary =  "FAIL"
}

# Plot the data sets
system('/usr/bin/gnuplot /opt/rtcds/rtscore/advligorts/test/rcgtest/perl/osc/oscTest.plt');

# Generate Test Report *****************************************************************************
open(OUTM,">/tmp/rcgtest/data/oscTest.dox") || die "cannot open test data file for writing";
print OUTM <<END;
/*!     \\page oscTest RCG Oscillator Test Data
*       \\verbatim
*************************************************************************************************

OSCILLATOR MODULE (OSC) CODE TEST REPORT 

TIME: $hour:$min:$sec  $abbr[$mon]  $day $year

RCG version number: $strEpicsVals[3]
SVN version number: $strEpicsVals[0]

*************************************************************************************************
PURPOSE: Verify the operation of RCG standard oscillator modules.

OVERVIEW:
This test sets various frequency values into an OSC part and measures output with FFT from a 
template DTT session. Results of the FFT are compared with the template measurements.

TEST REQUIREMENTS:
This test script (oscTest.pl) was designed to run on the LLO ATS test system. It requires that the
x2ats computer is running the x2atstim16.mdl software. The part under test is the TIM16_T1_OSC1 
oscillator part.

TEST PROCEDURE:
For each frequency listed below:
	- Set the oscillator frequency and amplitude.
	- Run diag using the appropriate template file.
	- Compare FFT results with that saved in the template file.
	- Produce a doxygen compatable test report.


TEST RESULTS:
Freq / Amp	  Frequency	Amplitude	Avg Amp		Delta Template	Errors	PASS/FAIL
								    Avg
----------------------------------------------------------------------------------------------------
  10Hz 2000 amp  - $tFreq[1]	$tAmp[1]	$tAvg[1]		$tAvgDiff[1]	$tErr[1]	$testRes[1]          
----------------------------------------------------------------------------------------------------
2000Hz 2000 amp  - $tFreq[0]	$tAmp[0]	$tAvg[0]		$tAvgDiff[0]	$tErr[0]	$testRes[0]          
----------------------------------------------------------------------------------------------------
6500Hz 2000 amp  - $tFreq[2]	$tAmp[2]	$tAvg[2]		$tAvgDiff[2]	$tErr[2]	$testRes[2]          

TEST SUMMARY:  $test_summary



        \\endverbatim

\\image html osc10.png "OSC10Hz Data"
\\image html osc2000.png "OSC2000Hz Data"
\\image html osc6500.png "OSC6500Hz Data"
\\image latex osc10.png "OSC10Hz Data"
\\image latex osc2000.png "OSC2000Hz Data"
\\image latex osc6500.png "OSC6500Hz Data"

*\/

END
if($status == 0) {
    caPut("X2:IOP-SAM_TEST_STATUS","OSC TEST SUCCESS");
    caPut("X2:IOP-SAM_TEST_STAT_9",1);
}
if($status == -1) {
    caPut("X2:IOP-SAM_TEST_STAT_9",0);
    caPut("X2:IOP-SAM_TEST_STATUS","OSC TEST FAILED");
}
sleep(5);

caPut("X2:IOP-SAM_TEST_STATUS","TEST SYSTEM - IDLE");
($sec,$min, $hour, $day, $mon, $year, $wday, $yday, $isdst) = localtime(time);
$year += 1900;
print  "COMPLETION TIME: $hour:$min:$sec  $abbr[$mon]  $day $year \n";

system("cat /tmp/rcgtest/data/oscTest.dox");
exit($status);


