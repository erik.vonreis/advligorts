#!/usr/bin/perl -w -I /opt/cdscfg
##
##
use threads;
use threads::shared;
BEGIN {
	$epicsbase = $ENV{'EPICS_BASE'};
        $epicslib = $epicsbase . "/lib/linux-x86_64";
        $epicsperl = $epicsbase . "/lib/perl";
        $rcgtestdir = $ENV{'RCG_TEST_DIR'};
        $rcgdir = $ENV{'RCG_DIR'};
        print "EPICSBASE = $epicsbase \n";
        print "EPICSLIB = $epicslib \n";
        print "EPICSPERL = $epicsperl \n";
        push @INC,"/opt/cdscfg/tst/stddir.pl";
        push @INC,"$epicsperl";
        push @INC,"$epicslib";
	 push @INC,"/opt/rtapps/debian10/perlmodules";
        # push @INC,"/opt/rtcds/userapps/trunk/cds/test/scripts";
        push @INC,"/usr/lib/x86_64-linux-gnu/perl5/5.28/auto";
        push @INC,"/usr/lib/x86_64-linux-gnu/perl5/5.28";
        push @INC,"/opt/rtcds/rtscore/advligorts/src/perldaq";
}
#use stdenv;
use CaTools;
use Time::HiRes qw( usleep );
use POSIX qw(ceil floor);
use File::Path;

use strict;
open(OUTM,">/tmp/rcgtest/data/fmc2TestData.dox") || die "cannot open test data file for writing";
(my $sec,my $min, my $hour, my $day, my $mon, my $year, my $wday, my $yday, my $isdst) = localtime(time);
$year += 1900;
my @abbr = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
my @strEpics = ("X2:FEC-112_RCG_VERSION","X2:FEC-112_MSG","X2:FEC-112_MSGDAQ","X2:FEC-112_RCG_VERSION");
my @strEpicsVals;
@strEpicsVals = caGet(@strEpics);


caPut("X2:IOP-SAM_TEST_STATUS","STARTING FMC2 TEST");
caPut("X2:IOP-SAM_TEST_STAT_11",2);
#print "\n\nSTARTING FMC2 TESTING ****************************************************************\n";
#print "TIME: $hour:$min:$sec  $abbr[$mon]  $day $year \n\n";
print  "\n\nSTARTING FMC2 TESTING ************************************************************************\n";
print  "TIME: $hour:$min:$sec  $abbr[$mon]  $day $year \n";
print "RCG Version: $strEpicsVals[3]\n";


my @ctrlVals;
my @ctrlMons = ("X2:ATS-TIM16_T3_CTRL_OUT",
                "X2:ATS-TIM16_T3_ADC_FILTER_17_MASK",
                "X2:ATS-TIM16_T3_OFFSET_OUT",
                "X2:ATS-TIM16_T3_GAIN_OUT",
                "X2:ATS-TIM16_T3_RAMP_OUT",
                "X2:ATS-TIM16_T3_ADC_FILTER_17_OUTPUT");
my @filtVals;
my @filtMons = ("X2:ATS-TIM16_T3_ADC_FILTER_17_OFFSET",
                "X2:ATS-TIM16_T3_ADC_FILTER_17_GAIN",
                "X2:ATS-TIM16_T3_ADC_FILTER_17_TRAMP");
my @filtValsInit = (0.0,1.0,1.0);

my $isok;
my $status = 0;
my $ii;
my $err = 0;
my $test_summary = "FAIL";


caPut("X2:ATS-TIM16_T3_CTRL_TST",0);
caPut("X2:ATS-TIM16_T3_MASK_TST",0);
caPut("X2:ATS-TIM16_T3_OFFSET_TST",0.0);
caPut("X2:ATS-TIM16_T3_GAIN_TST",1.0);
caPut("X2:ATS-TIM16_T3_RAMP_TST",1.0);
sleep(2);
# Initialize control settings
caPut(@filtMons,@filtValsInit);

# Set all of the filter switches to zero and check 
caSwitch("X2:ATS-TIM16_T3_ADC_FILTER_17","FM1 OFF FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT OFF OFFSET OFF OUTPUT OFF");
sleep(2);

@ctrlVals = caGet(@ctrlMons);
my @compVals = (0,0,0.0,1.0,1.0);
$err = 0;
for($ii=0;$ii<5;$ii++)
{
        if($ctrlVals[$ii] != $compVals[$ii]) { 
	$err ++; }

}
my $icsTest = "PASS";
if($err)
{
	$icsTest = "FAIL";
        $status = -1;;
}

my @swSet = (0x1,0x3,0x7,0xf,0x1f,0x3f,0x7f,0xff,0x1ff,0x3ff,0x7ff,0xfff,0x1fff);

caPut("X2:IOP-SAM_TEST_STATUS","FMC2 TEST - REMOTE CONTROL");
# Check control out word when switches set from normal EPICS input
$err = 0;
caSwitch("X2:ATS-TIM16_T3_ADC_FILTER_17","FM1 ON FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT OFF OFFSET OFF OUTPUT OFF");
sleep(2);
@ctrlVals = caGet(@ctrlMons);
if($ctrlVals[0] != $swSet[0]) { $err ++; }
caSwitch("X2:ATS-TIM16_T3_ADC_FILTER_17","FM1 ON FM2 ON FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT OFF OFFSET OFF OUTPUT OFF");
sleep(2);
@ctrlVals = caGet(@ctrlMons);
if($ctrlVals[0] != $swSet[1]) { $err ++; }
caSwitch("X2:ATS-TIM16_T3_ADC_FILTER_17","FM1 ON FM2 ON FM3 ON FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT OFF OFFSET OFF OUTPUT OFF");
sleep(2);
@ctrlVals = caGet(@ctrlMons);
if($ctrlVals[0] != $swSet[2]) { $err ++; }
caSwitch("X2:ATS-TIM16_T3_ADC_FILTER_17","FM1 ON FM2 ON FM3 ON FM4 ON FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT OFF OFFSET OFF OUTPUT OFF");
sleep(2);
@ctrlVals = caGet(@ctrlMons);
if($ctrlVals[0] != $swSet[3]) { $err ++; }
caSwitch("X2:ATS-TIM16_T3_ADC_FILTER_17","FM1 ON FM2 ON FM3 ON FM4 ON FM5 ON FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT OFF OFFSET OFF OUTPUT OFF");
sleep(2);
@ctrlVals = caGet(@ctrlMons);
if($ctrlVals[0] != $swSet[4]) { $err ++; }
caSwitch("X2:ATS-TIM16_T3_ADC_FILTER_17","FM1 ON FM2 ON FM3 ON FM4 ON FM5 ON FM6 ON FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT OFF OFFSET OFF OUTPUT OFF");
sleep(2);
@ctrlVals = caGet(@ctrlMons);
if($ctrlVals[0] != $swSet[5]) { $err ++; }
caSwitch("X2:ATS-TIM16_T3_ADC_FILTER_17","FM1 ON FM2 ON FM3 ON FM4 ON FM5 ON FM6 ON FM7 ON FM8 OFF FM9 OFF FM10 OFF INPUT OFF OFFSET OFF OUTPUT OFF");
sleep(2);
@ctrlVals = caGet(@ctrlMons);
if($ctrlVals[0] != $swSet[6]) { $err ++; }
caSwitch("X2:ATS-TIM16_T3_ADC_FILTER_17","FM1 ON FM2 ON FM3 ON FM4 ON FM5 ON FM6 ON FM7 ON FM8 ON FM9 OFF FM10 OFF INPUT OFF OFFSET OFF OUTPUT OFF");
sleep(2);
@ctrlVals = caGet(@ctrlMons);
if($ctrlVals[0] != $swSet[7]) { $err ++; }
caSwitch("X2:ATS-TIM16_T3_ADC_FILTER_17","FM1 ON FM2 ON FM3 ON FM4 ON FM5 ON FM6 ON FM7 ON FM8 ON FM9 ON FM10 OFF INPUT OFF OFFSET OFF OUTPUT OFF");
sleep(2);
@ctrlVals = caGet(@ctrlMons);
if($ctrlVals[0] != $swSet[8]) { $err ++; }
caSwitch("X2:ATS-TIM16_T3_ADC_FILTER_17","FM1 ON FM2 ON FM3 ON FM4 ON FM5 ON FM6 ON FM7 ON FM8 ON FM9 ON FM10 ON INPUT OFF OFFSET OFF OUTPUT OFF");
sleep(2);
@ctrlVals = caGet(@ctrlMons);
if($ctrlVals[0] != $swSet[9]) { $err ++; }
caSwitch("X2:ATS-TIM16_T3_ADC_FILTER_17","FM1 ON FM2 ON FM3 ON FM4 ON FM5 ON FM6 ON FM7 ON FM8 ON FM9 ON FM10 ON INPUT ON OFFSET OFF OUTPUT OFF");
sleep(2);
@ctrlVals = caGet(@ctrlMons);
if($ctrlVals[0] != $swSet[10]) { $err ++; }
caSwitch("X2:ATS-TIM16_T3_ADC_FILTER_17","FM1 ON FM2 ON FM3 ON FM4 ON FM5 ON FM6 ON FM7 ON FM8 ON FM9 ON FM10 ON INPUT ON OFFSET ON OUTPUT OFF");
sleep(2);
@ctrlVals = caGet(@ctrlMons);
if($ctrlVals[0] != $swSet[11]) { $err ++; }
caSwitch("X2:ATS-TIM16_T3_ADC_FILTER_17","FM1 ON FM2 ON FM3 ON FM4 ON FM5 ON FM6 ON FM7 ON FM8 ON FM9 ON FM10 ON INPUT ON OFFSET ON OUTPUT ON");
sleep(2);
@ctrlVals = caGet(@ctrlMons);
if($ctrlVals[0] != $swSet[12]) { $err ++; }

my $rcsTest = "PASS";
if($err)
{
	$rcsTest = "FAIL";
        $status = -1;;
}

caPut("X2:IOP-SAM_TEST_STATUS","FMC2 TEST - LOCAL SW CONTROL");
# Check control out word when switches set from LOCAL input **************************************************************
$err = 0;
# Force all switches offX2:ATS-TIM16_T3_ADC_FILTER_17
caPut("X2:ATS-TIM16_T3_CTRL_TST",0);
caPut("X2:ATS-TIM16_T3_MASK_TST",0x1fff);
sleep(2);
print "forcing \n";
@ctrlVals = caGet(@ctrlMons);
if(($ctrlVals[0] != 0) || ($ctrlVals[1] != 0x1fff)) { $err ++; }

#Setup some stuff to check Guardian error detection.
my $alarmset = "MINOR";
my $alarmclear = "NO_ALARM";
my $alarmNone = "NO_ALARM";
my $alarmHigh = "HIGH";
my $errg = 0;
my @sv;
my @gSet = (0x1,0x3,0x7,0xf,0x1f,0x3f,0x7f,0xff,0x1ff,0x3ff,0x7ff,0xfff,0x1fff,0x1fff);

#Initialize Guardian variables
caPut("X2:ATS-TIM16_T3_ADC_FILTER_17_SWMASK",0x0);
caPut("X2:ATS-TIM16_T3_ADC_FILTER_17_SWREQ",0x0);
caPut("X2:ATS-TIM16_T3_ADC_FILTER_17_SWSTAT.HSV",$alarmset);

for($ii=0;$ii<13;$ii++)
{
	caPut("X2:ATS-TIM16_T3_CTRL_TST",$swSet[$ii]);
	#caPut("X2:ATS-TIM16_T3_ADC_FILTER_17_SWMASK",$gSet[$ii]);
	sleep(1);
	@ctrlVals = caGet(@ctrlMons);
	if($ctrlVals[0] != $swSet[$ii]) { $err ++; }
	# my @tv = caGet("X2:ATS-TIM16_T3_ADC_FILTER_17_SWSTAT.STAT");
	# if($tv[0] ne $alarmHigh) {
		# print "Alarm error HIGH = $tv[0]\n";
		# $errg ++;
	# }
	# caPut("X2:ATS-TIM16_T3_ADC_FILTER_17_SWMASK",$gSet[$ii+1]);
	# caPut("X2:ATS-TIM16_T3_ADC_FILTER_17_SWREQ",$gSet[$ii]);
	# sleep(1);
	# @tv = caGet("X2:ATS-TIM16_T3_ADC_FILTER_17_SWSTAT.STAT");
	# if($tv[0] ne $alarmNone) {
		# print "Alarm error None = $tv[0]\n";
		# $errg ++;
	# }
@sv = caGet("X2:ATS-TIM16_T3_ADC_FILTER_17_SW1S");
print "Guard test $ii sw = $sv[0]\n";
}

my $lcsTest = "PASS";
my $guardTest = "PASS";

if($err)
{
	$lcsTest = "FAIL";
        $status = -1;;
}
if($errg)
{
	$guardTest = "FAIL";
        $status = -1;;
}

caPut("X2:IOP-SAM_TEST_STATUS","FMC2 TEST - LOCAL G/R/O CONTROL");
# Check Gains, Offsets and Ramp Time settings. ***************************************************************************
$err = 0;
caPut("X2:ATS-TIM16_T3_MASK_TST",0xffff);
sleep(2);
@ctrlVals = caGet(@ctrlMons);
if($ctrlVals[1] != 0xffff) { $err ++; }
caPut("X2:ATS-TIM16_T3_OFFSET_TST",2000.0);
sleep(2);
@filtVals = caGet(@filtMons);
if($filtVals[0] != 2000) { $err ++; }
caPut("X2:ATS-TIM16_T3_RAMP_TST",5.0);
sleep(2);
@filtVals = caGet(@filtMons);
if($filtVals[2] != 5) { $err ++; }
caPut("X2:ATS-TIM16_T3_GAIN_TST",2.0);
sleep(2);
@filtVals = caGet(@filtMons);
if($filtVals[1] != 2) { $err ++; }

my $lcTest = "PASS";

if($err)
{
	$lcTest = "FAIL";
        $status = -1;;
}
# Check Switching from local control to remote control  ***************************************************************************
$err = 0;
caPut("X2:ATS-TIM16_T3_MASK_TST",0x0);
sleep(2);
my $lrTest = "PASS";
@ctrlVals = caGet(@ctrlMons);
if($ctrlVals[1] != 0x0) { $err ++; }
if($ctrlVals[0] != 0x1fff) { $err ++; }

if($err)
{
	$lrTest = "FAIL";
        $status = -1;;
}

my $fastSwitchTest = "PASS";

if($status == 0) {
    $test_summary =  "PASS";
} else {
    $test_summary =  "FAIL";
}



# Print results ************************************************

print OUTM <<END;
/*!     \\page fmcControl2Test Filter Module w/Control 2 Test Data
*       \\verbatim
*************************************************************************************************

Filter Module w/Control 2 (FMC2) CODE TEST REPORT 

TIME: $hour:$min:$sec  $abbr[$mon]  $day $year

RCG version number: $strEpicsVals[3]

*************************************************************************************************

PURPOSE:
	Test local/remote switching of FMC2 part. This test also checks Guardian alarm notification
	capabilities for all standard filter modules.

TEST OVERVIEW:
	Tests local/remote control settings of FMC2 by:
		1) Using standard FMC2 EPICS channels (remote control) to set/switch FMC2 parameters.
		2) Using embedded EPICS channels (local control) to set/switch FMC2 parameters.

TEST REQUIREMENTS:
        This test was designed for use with the LLO DAQ Test Stand. It requires that the x1ats computer
	is running with the x2atstim16 model loaded.

TEST PROCEDURE:
        This test is automatically run by executing the fmc2test.pl script, which is located in the
        /opt/rtcds/rtscore/advligorts/test/rcgtest/perl/fmc2 directory.
		- Clear all switches
		- Switch in and test one FMC2 switch at a time using normal EPICS remote control.
		- Clear all switches
		- Switch in and test one FMC2 switch at a time using FMC local control inputs.
			- Set and test Guardian alarms for each switch setting.
		- Test remote control of FMC gains, offset and ramp settings.

TEST MEDM SCREEN:
        \\endverbatim

\\image html fmc2testscreen.png


*       \\verbatim
TEST RESULTS:

-------------------------------------------------------------------------------------------------
      TEST                      				PASS/FAIL
-------------------------------------------------------------------------------------------------

	- Initial control output values (0,0,0,1,1)		$icsTest

	- Control switches set REMOTE (EPICS)			$rcsTest

	- Control switches set LOCAL (RT Code)			$lcsTest

	- Filter Gain, Offset,Ramp LOCAL (RT Code) Control	$lcTest

	- Switch from LOCAL to REMOTE Control			$lrTest

	- Guardian Alarm Test			    		$guardTest

*************************************************************************************************

TEST SUMMARY:  $test_summary

        \\endverbatim
*\/


END
close OUTM;
print "TEST COMPLETE ***********************************************************************************\n";
system("cat /tmp/rcgtest/data/fmc2TestData.dox");
if($status == 0) { 
    caPut("X2:IOP-SAM_TEST_STATUS","FMC2 TEST SUCCESS");
    caPut("X2:IOP-SAM_TEST_STAT_11",1);
}
if($status == -1) {
    caPut("X2:IOP-SAM_TEST_STATUS","FMC2 TEST FAILED");
    caPut("X2:IOP-SAM_TEST_STAT_11",0);
}
sleep(5);
caPut("X2:IOP-SAM_TEST_STATUS","TEST SYSTEM - IDLE");
exit($status);
