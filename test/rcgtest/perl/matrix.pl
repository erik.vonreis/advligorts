#!/usr/bin/perl -w -I/opt/rtcds/rtscore/trunk/src/perldaq

use IO::Handle;
use POSIX;
BEGIN {

    $epicsbase = $ENV{'EPICS_BASE'};
         $epicslib = $epicsbase . "/lib/linux-x86_64";
         $epicsperl = $epicsbase . "/lib/perl";
         print "EPICSBASE = $epicsbase \n";
         print "EPICSLIB = $epicslib \n";
         print "EPICSPERL = $epicsperl \n";
         push @INC,"/opt/cdscfg/tst/stddir.pl";
         push @INC,"$epicsperl";
         push @INC,"$epicslib";
	push @INC,"/opt/rtapps/debian10/perlmodules";
        push @INC,"/usr/lib/x86_64-linux-gnu/perl5/5.28/auto";
        push @INC,"/usr/lib/x86_64-linux-gnu/perl5/5.28";
        push @INC,"/opt/rtcds/rtscore/advligorts/src/perldaq";
        push @INC,"/opt/rtcds/rtscore/advligorts/test/rcgtest/perl";
}
use CaTools;
use Time::HiRes qw( usleep );
use POSIX qw(ceil floor);
use File::Path;
use DtTools;


# Need daqperl module to get data from NDS
require "daq.pm";

$rcgtestdir = $ARGV[0];

$rcgtestdatadir = $rcgtestdir . "/data";
$rcgtestimagesdir = $rcgtestdir . "/images";

(my $sec,my $min, my $hour, my $day, my $mon, my $year, my $wday, my $yday, my $isdst) = localtime(time);
$year += 1900;
my @abbr = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
my @strEpics = ("X2:FEC-112_RCG_VERSION","X2:FEC-112_MSG","X2:FEC-112_MSGDAQ","X2:FEC-112_RCG_VERSION");
my @strEpicsVals;
@strEpicsVals = caGet(@strEpics);

print  "\n\nSTARTING MATRIX MODULE TESTING *********************************************\n";
print  "TIME: $hour:$min:$sec  $abbr[$mon]  $day $year \n";
print "RCG Version: $strEpicsVals[3]\n";


# Post Test Status Message to overview medm
caPut("X2:IOP-SAM_TEST_STATUS","MATRIX TEST ");
caPut("X2:IOP-SAM_TEST_STAT_8",2);



# Define the matrix output channels
my @matouts = (	"X2:ATS-TIM16_T1_M1OUT_0","X2:ATS-TIM16_T1_M1OUT_1","X2:ATS-TIM16_T1_M1OUT_2","X2:ATS-TIM16_T1_M1OUT_3",
		"X2:ATS-TIM16_T1_M1OUT_4","X2:ATS-TIM16_T1_M1OUT_5","X2:ATS-TIM16_T1_M1OUT_6","X2:ATS-TIM16_T1_M1OUT_7",
		"X2:ATS-TIM16_T1_M1OUT_8","X2:ATS-TIM16_T1_M1OUT_9","X2:ATS-TIM16_T1_M1OUT_10","X2:ATS-TIM16_T1_M1OUT_11",
		"X2:ATS-TIM16_T1_M1OUT_12","X2:ATS-TIM16_T1_M1OUT_13","X2:ATS-TIM16_T1_M1OUT_14","X2:ATS-TIM16_T1_M1OUT_15");
my @matOutVals;

# Define the 16x16 matrix channels
my @matrixElements1 =   ("X2:ATS-TIM16_T1_MATRIX1_1_1",  "X2:ATS-TIM16_T1_MATRIX1_1_2",  "X2:ATS-TIM16_T1_MATRIX1_1_3",  "X2:ATS-TIM16_T1_MATRIX1_1_4", 
			 "X2:ATS-TIM16_T1_MATRIX1_1_5",  "X2:ATS-TIM16_T1_MATRIX1_1_6",  "X2:ATS-TIM16_T1_MATRIX1_1_7",  "X2:ATS-TIM16_T1_MATRIX1_1_8", 
			 "X2:ATS-TIM16_T1_MATRIX1_1_9",  "X2:ATS-TIM16_T1_MATRIX1_1_10", "X2:ATS-TIM16_T1_MATRIX1_1_11", "X2:ATS-TIM16_T1_MATRIX1_1_12", 
			 "X2:ATS-TIM16_T1_MATRIX1_1_13", "X2:ATS-TIM16_T1_MATRIX1_1_14", "X2:ATS-TIM16_T1_MATRIX1_1_15", "X2:ATS-TIM16_T1_MATRIX1_1_16",
			 "X2:ATS-TIM16_T1_MATRIX1_2_1",  "X2:ATS-TIM16_T1_MATRIX1_2_2",  "X2:ATS-TIM16_T1_MATRIX1_2_3",  "X2:ATS-TIM16_T1_MATRIX1_2_4", 
			 "X2:ATS-TIM16_T1_MATRIX1_2_5",  "X2:ATS-TIM16_T1_MATRIX1_2_6",  "X2:ATS-TIM16_T1_MATRIX1_2_7",  "X2:ATS-TIM16_T1_MATRIX1_2_8", 
			 "X2:ATS-TIM16_T1_MATRIX1_2_9",  "X2:ATS-TIM16_T1_MATRIX1_2_10", "X2:ATS-TIM16_T1_MATRIX1_2_11", "X2:ATS-TIM16_T1_MATRIX1_2_12", 
			 "X2:ATS-TIM16_T1_MATRIX1_2_13", "X2:ATS-TIM16_T1_MATRIX1_2_14", "X2:ATS-TIM16_T1_MATRIX1_2_15", "X2:ATS-TIM16_T1_MATRIX1_2_16",
			 "X2:ATS-TIM16_T1_MATRIX1_3_1",  "X2:ATS-TIM16_T1_MATRIX1_3_2",  "X2:ATS-TIM16_T1_MATRIX1_3_3",  "X2:ATS-TIM16_T1_MATRIX1_3_4", 
			 "X2:ATS-TIM16_T1_MATRIX1_3_5",  "X2:ATS-TIM16_T1_MATRIX1_3_6",  "X2:ATS-TIM16_T1_MATRIX1_3_7",  "X2:ATS-TIM16_T1_MATRIX1_3_8", 
			 "X2:ATS-TIM16_T1_MATRIX1_3_9",  "X2:ATS-TIM16_T1_MATRIX1_3_10", "X2:ATS-TIM16_T1_MATRIX1_3_11", "X2:ATS-TIM16_T1_MATRIX1_3_12", 
			 "X2:ATS-TIM16_T1_MATRIX1_3_13", "X2:ATS-TIM16_T1_MATRIX1_3_14", "X2:ATS-TIM16_T1_MATRIX1_3_15", "X2:ATS-TIM16_T1_MATRIX1_3_16",
			 "X2:ATS-TIM16_T1_MATRIX1_4_1",  "X2:ATS-TIM16_T1_MATRIX1_4_2",  "X2:ATS-TIM16_T1_MATRIX1_4_3",  "X2:ATS-TIM16_T1_MATRIX1_4_4", 
			 "X2:ATS-TIM16_T1_MATRIX1_4_5",  "X2:ATS-TIM16_T1_MATRIX1_4_6",  "X2:ATS-TIM16_T1_MATRIX1_4_7",  "X2:ATS-TIM16_T1_MATRIX1_4_8", 
			 "X2:ATS-TIM16_T1_MATRIX1_4_9",  "X2:ATS-TIM16_T1_MATRIX1_4_10", "X2:ATS-TIM16_T1_MATRIX1_4_11", "X2:ATS-TIM16_T1_MATRIX1_4_12", 
			 "X2:ATS-TIM16_T1_MATRIX1_4_13", "X2:ATS-TIM16_T1_MATRIX1_4_14", "X2:ATS-TIM16_T1_MATRIX1_4_15", "X2:ATS-TIM16_T1_MATRIX1_4_16",
			 "X2:ATS-TIM16_T1_MATRIX1_5_1",  "X2:ATS-TIM16_T1_MATRIX1_5_2",  "X2:ATS-TIM16_T1_MATRIX1_5_3",  "X2:ATS-TIM16_T1_MATRIX1_5_4", 
			 "X2:ATS-TIM16_T1_MATRIX1_5_5",  "X2:ATS-TIM16_T1_MATRIX1_5_6",  "X2:ATS-TIM16_T1_MATRIX1_5_7",  "X2:ATS-TIM16_T1_MATRIX1_5_8", 
			 "X2:ATS-TIM16_T1_MATRIX1_5_9",  "X2:ATS-TIM16_T1_MATRIX1_5_10", "X2:ATS-TIM16_T1_MATRIX1_5_11", "X2:ATS-TIM16_T1_MATRIX1_5_12", 
			 "X2:ATS-TIM16_T1_MATRIX1_5_13", "X2:ATS-TIM16_T1_MATRIX1_5_14", "X2:ATS-TIM16_T1_MATRIX1_5_15", "X2:ATS-TIM16_T1_MATRIX1_5_16",
			 "X2:ATS-TIM16_T1_MATRIX1_6_1",  "X2:ATS-TIM16_T1_MATRIX1_6_2",  "X2:ATS-TIM16_T1_MATRIX1_6_3",  "X2:ATS-TIM16_T1_MATRIX1_6_4", 
			 "X2:ATS-TIM16_T1_MATRIX1_6_5",  "X2:ATS-TIM16_T1_MATRIX1_6_6",  "X2:ATS-TIM16_T1_MATRIX1_6_7",  "X2:ATS-TIM16_T1_MATRIX1_6_8", 
			 "X2:ATS-TIM16_T1_MATRIX1_6_9",  "X2:ATS-TIM16_T1_MATRIX1_6_10", "X2:ATS-TIM16_T1_MATRIX1_6_11", "X2:ATS-TIM16_T1_MATRIX1_6_12", 
			 "X2:ATS-TIM16_T1_MATRIX1_6_13", "X2:ATS-TIM16_T1_MATRIX1_6_14", "X2:ATS-TIM16_T1_MATRIX1_6_15", "X2:ATS-TIM16_T1_MATRIX1_6_16",
			 "X2:ATS-TIM16_T1_MATRIX1_7_1",  "X2:ATS-TIM16_T1_MATRIX1_7_2",  "X2:ATS-TIM16_T1_MATRIX1_7_3",  "X2:ATS-TIM16_T1_MATRIX1_7_4", 
			 "X2:ATS-TIM16_T1_MATRIX1_7_5",  "X2:ATS-TIM16_T1_MATRIX1_7_6",  "X2:ATS-TIM16_T1_MATRIX1_7_7",  "X2:ATS-TIM16_T1_MATRIX1_7_8", 
			 "X2:ATS-TIM16_T1_MATRIX1_7_9",  "X2:ATS-TIM16_T1_MATRIX1_7_10", "X2:ATS-TIM16_T1_MATRIX1_7_11", "X2:ATS-TIM16_T1_MATRIX1_7_12", 
			 "X2:ATS-TIM16_T1_MATRIX1_7_13", "X2:ATS-TIM16_T1_MATRIX1_7_14", "X2:ATS-TIM16_T1_MATRIX1_7_15", "X2:ATS-TIM16_T1_MATRIX1_7_16",
			 "X2:ATS-TIM16_T1_MATRIX1_8_1",  "X2:ATS-TIM16_T1_MATRIX1_8_2",  "X2:ATS-TIM16_T1_MATRIX1_8_3",  "X2:ATS-TIM16_T1_MATRIX1_8_4", 
			 "X2:ATS-TIM16_T1_MATRIX1_8_5",  "X2:ATS-TIM16_T1_MATRIX1_8_6",  "X2:ATS-TIM16_T1_MATRIX1_8_7",  "X2:ATS-TIM16_T1_MATRIX1_8_8", 
			 "X2:ATS-TIM16_T1_MATRIX1_8_9",  "X2:ATS-TIM16_T1_MATRIX1_8_10", "X2:ATS-TIM16_T1_MATRIX1_8_11", "X2:ATS-TIM16_T1_MATRIX1_8_12", 
			 "X2:ATS-TIM16_T1_MATRIX1_8_13", "X2:ATS-TIM16_T1_MATRIX1_8_14", "X2:ATS-TIM16_T1_MATRIX1_8_15", "X2:ATS-TIM16_T1_MATRIX1_8_16",
			 "X2:ATS-TIM16_T1_MATRIX1_9_1",  "X2:ATS-TIM16_T1_MATRIX1_9_2",  "X2:ATS-TIM16_T1_MATRIX1_9_3",  "X2:ATS-TIM16_T1_MATRIX1_9_4", 
			 "X2:ATS-TIM16_T1_MATRIX1_9_5",  "X2:ATS-TIM16_T1_MATRIX1_9_6",  "X2:ATS-TIM16_T1_MATRIX1_9_7",  "X2:ATS-TIM16_T1_MATRIX1_9_8", 
			 "X2:ATS-TIM16_T1_MATRIX1_9_9",  "X2:ATS-TIM16_T1_MATRIX1_9_10", "X2:ATS-TIM16_T1_MATRIX1_9_11", "X2:ATS-TIM16_T1_MATRIX1_9_12", 
			 "X2:ATS-TIM16_T1_MATRIX1_9_13", "X2:ATS-TIM16_T1_MATRIX1_9_14", "X2:ATS-TIM16_T1_MATRIX1_9_15", "X2:ATS-TIM16_T1_MATRIX1_9_16",
			 "X2:ATS-TIM16_T1_MATRIX1_10_1",  "X2:ATS-TIM16_T1_MATRIX1_10_2",  "X2:ATS-TIM16_T1_MATRIX1_10_3",  "X2:ATS-TIM16_T1_MATRIX1_10_4", 
			 "X2:ATS-TIM16_T1_MATRIX1_10_5",  "X2:ATS-TIM16_T1_MATRIX1_10_6",  "X2:ATS-TIM16_T1_MATRIX1_10_7",  "X2:ATS-TIM16_T1_MATRIX1_10_8", 
			 "X2:ATS-TIM16_T1_MATRIX1_10_9",  "X2:ATS-TIM16_T1_MATRIX1_10_10", "X2:ATS-TIM16_T1_MATRIX1_10_11", "X2:ATS-TIM16_T1_MATRIX1_10_12", 
			 "X2:ATS-TIM16_T1_MATRIX1_10_13", "X2:ATS-TIM16_T1_MATRIX1_10_14", "X2:ATS-TIM16_T1_MATRIX1_10_15", "X2:ATS-TIM16_T1_MATRIX1_10_16",
			 "X2:ATS-TIM16_T1_MATRIX1_11_1",  "X2:ATS-TIM16_T1_MATRIX1_11_2",  "X2:ATS-TIM16_T1_MATRIX1_11_3",  "X2:ATS-TIM16_T1_MATRIX1_11_4", 
			 "X2:ATS-TIM16_T1_MATRIX1_11_5",  "X2:ATS-TIM16_T1_MATRIX1_11_6",  "X2:ATS-TIM16_T1_MATRIX1_11_7",  "X2:ATS-TIM16_T1_MATRIX1_11_8", 
			 "X2:ATS-TIM16_T1_MATRIX1_11_9",  "X2:ATS-TIM16_T1_MATRIX1_11_10", "X2:ATS-TIM16_T1_MATRIX1_11_11", "X2:ATS-TIM16_T1_MATRIX1_11_12", 
			 "X2:ATS-TIM16_T1_MATRIX1_11_13", "X2:ATS-TIM16_T1_MATRIX1_11_14", "X2:ATS-TIM16_T1_MATRIX1_11_15", "X2:ATS-TIM16_T1_MATRIX1_11_16",
			 "X2:ATS-TIM16_T1_MATRIX1_12_1",  "X2:ATS-TIM16_T1_MATRIX1_12_2",  "X2:ATS-TIM16_T1_MATRIX1_12_3",  "X2:ATS-TIM16_T1_MATRIX1_12_4", 
			 "X2:ATS-TIM16_T1_MATRIX1_12_5",  "X2:ATS-TIM16_T1_MATRIX1_12_6",  "X2:ATS-TIM16_T1_MATRIX1_12_7",  "X2:ATS-TIM16_T1_MATRIX1_12_8", 
			 "X2:ATS-TIM16_T1_MATRIX1_12_9",  "X2:ATS-TIM16_T1_MATRIX1_12_10", "X2:ATS-TIM16_T1_MATRIX1_12_11", "X2:ATS-TIM16_T1_MATRIX1_12_12", 
			 "X2:ATS-TIM16_T1_MATRIX1_12_13", "X2:ATS-TIM16_T1_MATRIX1_12_14", "X2:ATS-TIM16_T1_MATRIX1_12_15", "X2:ATS-TIM16_T1_MATRIX1_12_16",
			 "X2:ATS-TIM16_T1_MATRIX1_13_1",  "X2:ATS-TIM16_T1_MATRIX1_13_2",  "X2:ATS-TIM16_T1_MATRIX1_13_3",  "X2:ATS-TIM16_T1_MATRIX1_13_4", 
			 "X2:ATS-TIM16_T1_MATRIX1_13_5",  "X2:ATS-TIM16_T1_MATRIX1_13_6",  "X2:ATS-TIM16_T1_MATRIX1_13_7",  "X2:ATS-TIM16_T1_MATRIX1_13_8", 
			 "X2:ATS-TIM16_T1_MATRIX1_13_9",  "X2:ATS-TIM16_T1_MATRIX1_13_10", "X2:ATS-TIM16_T1_MATRIX1_13_11", "X2:ATS-TIM16_T1_MATRIX1_13_12", 
			 "X2:ATS-TIM16_T1_MATRIX1_13_13", "X2:ATS-TIM16_T1_MATRIX1_13_14", "X2:ATS-TIM16_T1_MATRIX1_13_15", "X2:ATS-TIM16_T1_MATRIX1_13_16",
			 "X2:ATS-TIM16_T1_MATRIX1_14_1",  "X2:ATS-TIM16_T1_MATRIX1_14_2",  "X2:ATS-TIM16_T1_MATRIX1_14_3",  "X2:ATS-TIM16_T1_MATRIX1_14_4", 
			 "X2:ATS-TIM16_T1_MATRIX1_14_5",  "X2:ATS-TIM16_T1_MATRIX1_14_6",  "X2:ATS-TIM16_T1_MATRIX1_14_7",  "X2:ATS-TIM16_T1_MATRIX1_14_8", 
			 "X2:ATS-TIM16_T1_MATRIX1_14_9",  "X2:ATS-TIM16_T1_MATRIX1_14_10", "X2:ATS-TIM16_T1_MATRIX1_14_11", "X2:ATS-TIM16_T1_MATRIX1_14_12", 
			 "X2:ATS-TIM16_T1_MATRIX1_14_13", "X2:ATS-TIM16_T1_MATRIX1_14_14", "X2:ATS-TIM16_T1_MATRIX1_14_15", "X2:ATS-TIM16_T1_MATRIX1_14_16",
			 "X2:ATS-TIM16_T1_MATRIX1_15_1",  "X2:ATS-TIM16_T1_MATRIX1_15_2",  "X2:ATS-TIM16_T1_MATRIX1_15_3",  "X2:ATS-TIM16_T1_MATRIX1_15_4", 
			 "X2:ATS-TIM16_T1_MATRIX1_15_5",  "X2:ATS-TIM16_T1_MATRIX1_15_6",  "X2:ATS-TIM16_T1_MATRIX1_15_7",  "X2:ATS-TIM16_T1_MATRIX1_15_8", 
			 "X2:ATS-TIM16_T1_MATRIX1_15_9",  "X2:ATS-TIM16_T1_MATRIX1_15_10", "X2:ATS-TIM16_T1_MATRIX1_15_11", "X2:ATS-TIM16_T1_MATRIX1_15_12", 
			 "X2:ATS-TIM16_T1_MATRIX1_15_13", "X2:ATS-TIM16_T1_MATRIX1_15_14", "X2:ATS-TIM16_T1_MATRIX1_15_15", "X2:ATS-TIM16_T1_MATRIX1_15_16",
			 "X2:ATS-TIM16_T1_MATRIX1_16_1",  "X2:ATS-TIM16_T1_MATRIX1_16_2",  "X2:ATS-TIM16_T1_MATRIX1_16_3",  "X2:ATS-TIM16_T1_MATRIX1_16_4", 
			 "X2:ATS-TIM16_T1_MATRIX1_16_5",  "X2:ATS-TIM16_T1_MATRIX1_16_6",  "X2:ATS-TIM16_T1_MATRIX1_16_7",  "X2:ATS-TIM16_T1_MATRIX1_16_8", 
			 "X2:ATS-TIM16_T1_MATRIX1_16_9",  "X2:ATS-TIM16_T1_MATRIX1_16_10", "X2:ATS-TIM16_T1_MATRIX1_16_11", "X2:ATS-TIM16_T1_MATRIX1_16_12", 
			 "X2:ATS-TIM16_T1_MATRIX1_16_13", "X2:ATS-TIM16_T1_MATRIX1_16_14", "X2:ATS-TIM16_T1_MATRIX1_16_15", "X2:ATS-TIM16_T1_MATRIX1_16_16");


# Define the matrix input values
my @epicsValsAll = (10,20,30,40,50,60,70,80,90,100,110,120,130,140,150,160);
# Define the matrix input channels.
my @chansAll = ("X2:ATS-TIM16_T1_M1IN_0", "X2:ATS-TIM16_T1_M1IN_1", "X2:ATS-TIM16_T1_M1IN_2", "X2:ATS-TIM16_T1_M1IN_3",
	     "X2:ATS-TIM16_T1_M1IN_4", "X2:ATS-TIM16_T1_M1IN_5", "X2:ATS-TIM16_T1_M1IN_6", "X2:ATS-TIM16_T1_M1IN_7",
	     "X2:ATS-TIM16_T1_M1IN_8", "X2:ATS-TIM16_T1_M1IN_9", "X2:ATS-TIM16_T1_M1IN_10", "X2:ATS-TIM16_T1_M1IN_11",
	     "X2:ATS-TIM16_T1_M1IN_12", "X2:ATS-TIM16_T1_M1IN_13", "X2:ATS-TIM16_T1_M1IN_14", "X2:ATS-TIM16_T1_M1IN_15");

# Define clear matrix values ie all zeros
my @clrVals1 =  (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

#Define the matrix with diagonal one values
my @diagVals1 =  (1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		 0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		 0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,
		 0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,
		 0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,
		 0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,
		 0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,
		 0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,
		 0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,
		 0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,
		 0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,
		 0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,
		 0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,
		 0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,
		 0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,
		 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1);

# Define overall test status; this is returned to Jenkins on completion - zero if ok, -1 if fail
my $status = 0;
# Define the expected test results for comparison with later readbacks.
my @testVals0 = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @testVals1 = (10,20,30,40,50,60,70,80,90,100,110,120,130,140,150,160);
my @testVals2 = (13600,35360,57120,78880,100640,122400,144160,165920,187680,209440,231200,252960,274720,296480,318240,340000);

# Set status of all tests to PASS to start; will be set to FAIL if a test detects an error.
my @teststatus = qw(PASS PASS PASS PASS PASS PASS PASS PASS PASS PASS);


# Test 1 - Clear matrix and verify zero outputs. *************************************************************************
caPut(@matrixElements1,@clrVals1);
caPut(@chansAll,@epicsValsAll);
sleep(1);
	@matOutVals = caGet(@matouts);
print "Matrix Test 1: Verify matrix clear - \n"; 
for($jj=0;$jj<16;$jj++)
{
	if($testVals0[$jj] != $matOutVals[$jj]) { 
		print "Test error - expected $testVals1[$jj] and got $matOutVals[$jj] \n"; 
		$status = -1;
		$teststatus[0] = "FAIL";
	}
}
print "\t$teststatus[0] \n";

# Test 2 - Load rolling ones into matrix rows  *************************************************************************
# Test Rows of 16x16 matrix
print "Matrix Test 2: Verify Rows -  \n"; 
for($jj=0;$jj<16;$jj++)
{
caPut(@chansAll,@epicsValsAll);
sleep(1);
for($ii=0;$ii<16;$ii++)
{
	# Test with individual elements set to 1
	$kk = $ii + ($jj * 16);
	caPut($matrixElements1[$kk],1);
	if($ii>0) {caPut($matrixElements1[$kk-1],0);}
	sleep(1);
	@matOutVals = caGet(@matouts);
	#print "Matrix out $matouts[$jj] is $matOutVals[$jj] \n";
	if($testVals1[$ii] != $matOutVals[$jj]) { 
		print "Test error - expected $testVals1[$ii] and got $matOutVals[$kk] \n";
		$status = -1;
		$teststatus[1] = "FAIL";
	}
	# Test entire row set to 1
	if($ii == 15)
	{
		for($aa=0;$aa<16;$aa++)
		{
			$bb = $aa + ($jj * 16);
			caPut($matrixElements1[$bb],1);
		}
		sleep(1);
		@matOutVals = caGet(@matouts);
		if(1360  != $matOutVals[$jj]) { 
			print "Test error - expected 1360 and got $matOutVals[$jj] \n";
			$status = -1;
			$teststatus[1] = "FAIL";
		}
	}
}
}
print "\t$teststatus[1] \n";

# Clear matrix 
caPut(@matrixElements1,@clrVals1);
sleep(1);

# Test 3 - Load rolling ones into matrix cols  *************************************************************************
# Test Columns of 16x16 Matrix
print "Matrix Test 3: Verify Columns -  \n"; 
for($jj=0;$jj<16;$jj++)
{
    for($ii=0;$ii<16;$ii++)
    {
	# Test with individual elements set to 1
	$kk = ($ii * 16) + $jj ;
	caPut($matrixElements1[$kk],1);
	if($ii>0) {caPut($matrixElements1[$kk-16],0);}
	sleep(1);
	@matOutVals = caGet(@matouts);
	# print "Matrix out $matouts[$ii] is $matOutVals[$ii] \n";
	if($testVals1[$jj] != $matOutVals[$ii]) { 
		print "Test error - expected $testVals1[$ii] and got $matOutVals[$ii] \n";
		$status = -1;
		$teststatus[2] = "FAIL";
	}
	# Test entire col set to 1
	if($ii == 15)
	{
		for($aa=0;$aa<16;$aa++)
		{
			$bb = $jj + ($aa * 16);
			caPut($matrixElements1[$bb],1);
		}
		sleep(1);
		@matOutVals = caGet(@matouts);
		for($aa=0;$aa<16;$aa++)
		{
			if($testVals1[$jj] != $matOutVals[$aa]) { 
				print "Test error - expected $testVals1[$aa]  and got $matOutVals[$aa] \n";
				$status = -1;
				$teststatus[2] = "FAIL";
			}
		}
	}
    }
    # Clear matrix
    caPut(@matrixElements1,@clrVals1);
    sleep(1);
}
print "\t$teststatus[2] \n";

# Test 4 - Load diagonal ones into matrix cols  *************************************************************************
print "Matrix Test 4: Verify Diagonals -  \n"; 
sleep(1);
# Load diagonal one values
caPut(@matrixElements1,@diagVals1);
sleep(1);
@matOutVals = caGet(@matouts);
for($jj=0;$jj<16;$jj++)
{
	if($testVals1[$jj] != $matOutVals[$jj]) { 
		print "Test error - expected $testVals1[$jj] and got $matOutVals[$jj] \n";
		$status = -1;
		$teststatus[3] = "FAIL";
	}
}
print "\t$teststatus[3] \n";

# Test 5 - Load entire matrix with different values and verify outputs *************************************************
print "Matrix Test 5: Verify Full Matrix Settings \n"; 
for($ii=0;$ii<256;$ii++)
{
			caPut($matrixElements1[$ii],$ii);
}
		sleep(1);
		@matOutVals = caGet(@matouts);
for($ii=0;$ii<16;$ii++)
{
	print "Mat out $ii = $matOutVals[$ii] \n";
	if($testVals2[$ii] != $matOutVals[$ii]) { 
		print "Test error - expected $testVals2[$ii] and got $matOutVals[$ii] \n"; 
		$status = -1;
		$teststatus[4] = "FAIL";
	}
}
print "\t$teststatus[4] \n";


# Test 6 - With entire matrix with different values, verify DAQ has correct values *************************************
print "Matrix Test 6: Verify DAQ Channels -  \n"; 

# get the channel list
DAQ::connect("localhost", 8088);
sleep(4);

# get current time
$gpsdaq = DAQ::gps();
print "Waiting for DAQ Data \n";
for($ii=0;$ii<150;$ii++)
{
$timeRemaining = 150 - $ii;
caPut("X2:IOP-SAM_TEST_STATUS","MATRIX - DAQ WAIT $timeRemaining SECONDS ");
sleep(1);
}

print "Reading DAQ Data \n";
caPut("X2:IOP-SAM_TEST_STATUS","MATRIX - Read DAQ Data ");
# Read the matrix input values
for($ii=0;$ii<16;$ii++)
{
	my @inputdata = DAQ::acquire($chansAll[$ii], 1, $gpsdaq);
	if($inputdata[0] != $epicsValsAll[$ii]) { 
		print "Test error - $chansAll[$ii] expected $epicsValsAll[$ii] and got $inputdata[0] \n"; 
		$status = -1;
		$teststatus[5] = "FAIL";
	}
}
# Read the matrix element values
for($ii=0;$ii<256;$ii++)
{
	my @matrixdata = DAQ::acquire($matrixElements1[$ii], 1, $gpsdaq);
	#print "DAQ matrix $ii = $matrixdata[0] \n";
	if($matrixdata[0] != $ii) { 
		print "Test error - $matrixElements1[$ii] expected $ii and got $matrixdata[0] \n"; 
		$status = -1;
		$teststatus[6] = "FAIL";
	}
}
# Read the matrix output values
for($ii=0;$ii<16;$ii++)
{
	my @outputdata = DAQ::acquire($matouts[$ii], 1, $gpsdaq);
	if($outputdata[0] != $testVals2[$ii]) { 
		print "Test error - $matouts[$ii] expected $testVals2[$ii] and got $outputdata[0] \n"; 
		$status = -1;
		$teststatus[7] = "FAIL";
	}
}
print "\t$teststatus[6] \n";
if($status == 0) {
    $test_summary =  "PASS";
} else {
    $test_summary =  "FAIL";
}


# Testing complete - File test report **************************************************************************
$testdatafile = $rcgtestdatadir . "/matrixTestData.dox";
print "datafile: $testdatafile\n";
open(OUTM,'>', $testdatafile) || die "cannot open test data file for writing";

print OUTM <<END;
/*!     \\page matrixTest Matrix Test Results
*       \\verbatim
*************************************************************************************************

RCG MATRIX TEST REPORT 

TIME: $hour:$min:$sec  $abbr[$mon]  $day $year

RCG version number: $strEpicsVals[3]

*************************************************************************************************

PURPOSE: The purpose of this test is to verify the proper operation of the RCG MuxMatrix part.

TEST OVERVIEW:
	This test loads various inputs to a 16x16 matrix, sets the various matrix elements, and
	verifies that the matrix outputs are correct.

TEST REQUIREMENTS:
        This test is designed to run on the LLO DAQ test system, and requires:
                - DAQ system operational.
                - Model x2atstim16 running on x2ats computer.
                - Test script: /opt/rtcds/userapps/trunk/cds/test/scripts/matrix.pl

TEST PROCEDURE:
This test is of 6 parts:
	1) Clear the matrix and verify that all outputs are zero.
	2) Put a "rolling" 1 into each row element of the matrix, followed by a 1 in all elements of a row, and verify output.
	3) Put a "rolling" 1 into each column element of the matrix, followed by a 1 in all elements of a column, and verify output.
	4) Put a 1 in diagonal elements of the matrix and verify the outputs.
	5) Fully load the matrix with 0 to 255 and verify the outputs.
	6) With matrix loaded, verify DAQ channels match matrix settings.
		a) For EPICS matrix input channels
		b) For EPICS matrix settings
		c) For EPICS matrix output channels

RELATED MEDM SCREEN: X1ATS_TIM16_T1_MATRIX1_CUSTOM.adl
       \\endverbatim
\\image html matrixTest.png
\\image latex matrixTest.png

*       \\verbatim
TEST MODEL MODEL: x1fe3tim16
       \\endverbatim
\\image html matrixTestModel.png
\\image latex matrixTestModel.png

*       \\verbatim
TEST RESULTS:

-------------------------------------------------------------------------------------------------
      TEST                                        PASS/FAIL
-------------------------------------------------------------------------------------------------
1) Matrix Clear All Elements			$teststatus[0]
2) Matrix Column Test				$teststatus[1]
3) Matrix Row Test				$teststatus[2]
4) Matrix Diagonal Test				$teststatus[3]
5) Matrix Full Load Test			$teststatus[4]
6) Matrix DAQ Test				
	a) Input Values				$teststatus[5]
	b) Matrix Values			$teststatus[6]
	c) Output Values			$teststatus[7]

TEST SUMMARY:  $test_summary
       \\endverbatim

*\/

END
close OUTM;
system("cat  $testdatafile");

# Reset system test status on overview medm
caPut("X2:IOP-SAM_TEST_STATUS","TEST SYSTEM - IDLE ");
if($status == 0) {
    caPut("X2:IOP-SAM_TEST_STAT_8",1);
	print "Matrix Test Complete - PASS \n";
}else{
    caPut("X2:IOP-SAM_TEST_STAT_8",0);
	print "Matrix Test Complete - FAIL \n";
}

exit($status);

