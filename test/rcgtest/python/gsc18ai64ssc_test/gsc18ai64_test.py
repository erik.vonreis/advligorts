#!/usr/bin/env python
# Test report generation routines

from subprocess import call
from epics import PV
from epics import caget, caput
from math import sin,cos,radians
import sys
import time
sys.path.insert(0,'/opt/rtcds/rtscore/advligorts/rcgtest/python')
from cdstestsupport import testreport
from cdstestsupport import dtt, SFM

tr = testreport('adctest','DAC Order on Bus',112)
# Standard Header for all Test Scripts ****************************************************************
tr.sectionhead('Purpose')
s = ' Verify IOP passes proper DAC module mapping to user models. \n'
tr.sectiontext(s)

tr.sectionhead('Overview')
s = 'For RTS V4.1, the RCG was modified to allow various models of DAC cards \n'
s += 'to be installed in any order on the PCIe bus. This test is designed to \n'
s += 'to verify that user models get the proper mapping \n'
tr.sectiontext(s)

tr.sectionhead('Test Requirements')
s = 'This test was designed to run on the LLO DTS on x2ats:\n'
s += '  - Models x2iopsam, x2atstim32, x2atstim16, x2atstim04 and x2atstim02 \n'
s += '    must be running on x2ats. \n'
tr.sectiontext(s)


tr.sectionhead('Related MEDM Screen')
s = 'Test MEDM screen is at /opt/rtcds/rtscore/advligorts/rcgtest/medm/DAC_TEST.adl \n'
tr.sectiontext(s)
# tr.image('rampmuxmatrixMedm')
# END Standard Header for all Test Scripts ************************************************************

tr.sectionhead('Test Procedure')
s = 'The basic procedure is to drive various offsets in filters connected to DAC'
s += 'outputs and verify that control model and IOP model agree on which DAC'
s += 'module and channel receive the drive signal. '
tr.sectiontext(s)

pdiag = dtt()
# Start the test ******************************************

ii = 0;

tr.teststate.value = 'DAC PART TEST'

testvals = [1.0,2.0,3.0,4.0]
# List is 18_0, 20_0, 16_0 , 18_1, 18_2
iopcard = ['D18-0','D18-0','D20-0','D20-0','D16-0','D16-0',
            'D18-1','D18-1','D18-2','D18-2','D16-0','D18-2']
dacdrive = [1310,5242]
# Drive signals
filters = ['X2:ATS-TIM64_T1_DAC_FILTER_1','X2:ATS-TIM64_T1_DAC_FILTER_2']

iopadc = ['X2:IOP-SAM_MADC3_EPICS_CH0','X2:IOP-SAM_MADC3_EPICS_CH1',
            'X2:IOP-SAM_MADC3_EPICS_CH2','X2:IOP-SAM_MADC3_EPICS_CH3',
            'X2:IOP-SAM_MADC3_EPICS_CH4','X2:IOP-SAM_MADC3_EPICS_CH5',
            'X2:IOP-SAM_MADC3_EPICS_CH6','X2:IOP-SAM_MADC3_EPICS_CH7',
            'X2:IOP-SAM_MADC3_EPICS_CH8','X2:IOP-SAM_MADC3_EPICS_CH9',
            'X2:IOP-SAM_MADC3_EPICS_CH10','X2:IOP-SAM_MADC3_EPICS_CH11',
            'X2:IOP-SAM_MADC3_EPICS_CH12','X2:IOP-SAM_MADC3_EPICS_CH13',
            'X2:IOP-SAM_MADC3_EPICS_CH14','X2:IOP-SAM_MADC3_EPICS_CH15']




tr.teststatus == 0
#STEP 1 ***************************************************************************************************
tr.sectionhead('Step 1: Initialize Test Settings')
tr.teststeptxt = '\tInitialize all of the DAC driver filter modules to:\n'
tr.teststeptxt += '\t\t-Input Switch OFF\n'
tr.teststeptxt += '\t\t-Offset Switch ON\n'
tr.teststeptxt += '\t\t-Offset Value 2000.0\n'
tr.teststeptxt += '\t\t-Gain Value 1.0\n'
tr.teststeptxt += '\t\t-Limit Switch OFF\n'
tr.teststeptxt += '\t\t-Output Switch OFF\n'
tr.sectionstep()
# Clear all DAC outputs *************************************************
for ii in range (0,2):
    dacmodelfilter = SFM( filters[ii])
    dacmodelfilter.inputsw('OFF')
    dacmodelfilter.gain.value = 0.0
    dacmodelfilter.offset.value = dacdrive[ii]
    dacmodelfilter.offsetsw('ON')
    dacmodelfilter.limitsw('OFF')
    dacmodelfilter.outputsw('ON')

time.sleep(2)

#STEP 2 *******************************************************************************
tr.sectionhead('Step 2: Verify Channel Mapping')
tr.teststeptxt = '\tRun through 12 DAC channels, a few from each model:\n'
tr.teststeptxt += '\t\t-Turn ON one output at a time.\n'
tr.teststeptxt += '\t\t-Turn OFF remaining DAC outputs.\n'
tr.teststeptxt += '\t\t-Read DAC drive signals both from driving control model and IOP.\n'
tr.teststeptxt += '\t\t-Read ADC channels associated with DAC output signals.\n'
tr.teststeptxt += '\t\t\t - Verify IOP and control models show drive on proper channels.\n'
tr.teststeptxt += '\t\t\t - Verify IOP and control models show zero drive on OFF channels.\n'
tr.teststeptxt += '\t\t\t - Verify associated IOP ADC channels show proper signal.\n'
tststatus = "PASS"
localdata = []
iopdata = []
iopadcdata = []
for ii in range (0,16):
    iopadcdata = []
    gain = 0.0
    for kk in range (0,2):
        dacmodelfilter = SFM( filters[kk])
        dacmodelfilter.gain.value = gain
    s = ""
    myfile = "./adc18_ch"
    myfile += repr(ii)
    myfile += ".txt"
    f = open(myfile,"w")
    time.sleep(3)
    for jj in range (0,11):
        t = repr(jj)
        t += "    "
        testadc = PV(iopadc[ii])
        iopadcread = int(testadc.value)
        iopadcdata.append(iopadcread)
        s += repr(iopadcread)
        t += repr(iopadcread)
        t += "\n"
        s+= "  "
        f.write(t)
        gain += 1.0
        for kk in range (0,2):
            dacmodelfilter = SFM( filters[kk])
            dacmodelfilter.gain.value = gain
        time.sleep(2)
    s += "\n"
    f.close
    print s
    s = ""

    time.sleep(2)

for kk in range (0,2):
    dacmodelfilter = SFM( filters[kk])
    dacmodelfilter.gain.value = 0.0
tr.sectionstep()


if(tr.teststatus == 0):
    tr.sectionhead('TEST SUMMARY:  PASS') # *********************************************
else:
    tr.sectionhead('TEST SUMMARY:  FAIL') #*********************************************
#tr.sectionstep()



# Cleanup
#Clear the test runing flag
if tr.teststatus == 0:
    print "Test Pass \n"
else:
    print "Test Fail \n"

tr.close()
# Turn INPUT switch back ON to leave FM in default state
tr.teststate.value = 'TEST SYSTEM - IDLE'
call(["cat","/tmp/rcgtest/data/dactest.dox"])
sys.exit(tr.teststatus)

