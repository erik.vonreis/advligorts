set term png
set title "Time Series"
set xlabel 'Cycle Count'
set ylabel 'Magnitude (Counts)'
set style line 100 lt 1 lc rgb "gray" lw 2
set style line 101 lt 1 lc rgb "gray" lw 1
set grid xtics ls 100
set grid ytics ls 100

set output "/tmp/rcgtest/images/rmmRamp.png"
plot "/tmp/rcgtest/tmp/rampReference.data" using 1:2 smooth unique title "Reference Data", "/tmp/rcgtest/tmp/ramp.data" using 1:2 smooth unique title "Test Data"
