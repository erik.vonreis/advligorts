#!/bin/bash

rtcds build x2iopsam
rtcds install x2iopsam
rtcds build x2atstim32
rtcds install x2atstim32
rtcds build x2atstim16
rtcds install x2atstim16
rtcds build x2atstim04
rtcds install x2atstim04
rtcds build x2atstim02
rtcds install x2atstim02


rtcds build x2iopsa1
rtcds install x2iops1
rtcds build x2iopsa2
rtcds install x2iops2

rtcds build x2iops1pps
rtcds install x2iops1pps
rtcds build x2iops128
rtcds install x2iops128
rtcds build x2iops512
rtcds install x2iops512
