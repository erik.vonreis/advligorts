package fe_simulation

/*
#cgo pkg-config: fe_simulation

#include <stdlib.h>
#include <drv/shmem.h>
#include <daqmap.h>
#include <daq_core.h>
#include <stdint.h>
#include <shmem_all.h>
*/
import "C"
import (
	"errors"
	"fmt"
	"math/rand"
	"sort"
	"unsafe"
)

type daqMultiCycleHeader C.daq_multi_cycle_header_t
type daqDCData C.daq_dc_data_t

// Shmem is a wrapper around the LIGO shmem library which
// provides access to mbuf or posix shared memory segments
type Shmem struct {
	handle  C.shmem_handle
	mapping uintptr
}

// NewShmem Create/attach to a shared memory window
// name is the name of the buffer and may be prefixed with
// mbuf:// or shm:// to denote the type.
// sizeMB is the size of the buffer in MB
func NewShmem(name string, sizeMB int) (*Shmem, error) {
	s := &Shmem{}
	if err := s.open(name, sizeMB); err == nil {
		return s, nil
	} else {
		return nil, err
	}
}

func (s *Shmem) open(name string, sizeMB int) error {
	cName := C.CString(name)
	defer C.free(unsafe.Pointer(cName))
	s.handle = C.shmem_open(cName, C.size_t(sizeMB))
	if s.handle == nil {
		return errors.New("Unable to open shmem")
	}
	s.mapping = uintptr(C.shmem_mapping(s.handle))
	return nil
}

func (s *Shmem) Close() error {
	if s != nil {
		C.shmem_close(s.handle)
		s.handle = nil
	}
	return nil
}

// Mapping returns a uintptr that references the start of the
// shared memory buffer in this programs address space
func (s *Shmem) Mapping() uintptr {
	return s.mapping
}

func (s *Shmem) SizeMB() int {
	return int(C.shmem_size_mb(s.handle))
}

func (s *Shmem) Size() int {
	return s.SizeMB() * 1024 * 1024
}

// Model represents a simulated LIGO control model.
// Note this does not do any control, it is just a container
// for signal generators.
type Model struct {
	Name         string
	DcuId        int
	ModelRate    RateHertz
	DataRate     RateBytes
	ConfigCrc    uint32
	Status       int
	TPTable      []int
	Generators   []Generator
	TPGenerators []Generator
	NullTP       Generator

	ShmemObj     *Shmem
	rmipc        uintptr

	TP_Shmem     *Shmem
	TP_cfg		 *C.struct_TESTPOINT_CFG

	AWG_Shmem    *Shmem
	AWG_data     *C.struct_AWG_DATA

	ShmemPrefix  string
	lastNum      int
	lastChNum    int
}

type chNumDb struct {
	max int
}

func newDefaultChNumDb() *chNumDb {
	return &chNumDb{max: 0}
}

func newChNumDb(max int) *chNumDb {
	return &chNumDb{max: max}
}

func (d *chNumDb) Next() int {
	d.max++
	return d.max
}

// RateHertz is a strong type to help denote model rate (ie frequency)
// and keep it distinct when it is near data rates.
type RateHertz struct {
	Rate int
}

func NewRateHertz(rate int) RateHertz {
	return RateHertz{Rate: rate}
}

// RateBytes is a strong type to help denote model data rate (bytes)
// and keep it distinct when it is near model rates (frequency).
type RateBytes struct {
	Rate int
}

func NewRateBytes(rate int) RateBytes {
	return RateBytes{Rate: rate}
}

func cleanTpList(tpList []int) []int {
	out := make([]int, 0, C.DAQ_GDS_MAX_TP_NUM)
	for _, entry := range tpList {
		if entry > 0 && entry < 32 {
			out = append(out, entry)
		}
	}
	return out
}

func NewModel(iniManager *IniManager, name string,
	DcuId int, modelRate RateHertz, dataRate RateBytes, tpList []int, status int,
	shmemPrefix string) (*Model, error) {

	const testPoints = 32
	const slowRate = 16

	slowRateHertz := NewRateHertz(slowRate)

	floatSize := int(unsafe.Sizeof(C.float(0)))

	fastDataBytes := dataRate.Rate / 2
	midDataBytes := dataRate.Rate / 4
	slowDataBytes := dataRate.Rate / 4

	if fastDataBytes+midDataBytes+slowDataBytes != dataRate.Rate {
		return nil, errors.New("data rates do not add up when building model")
	}
	midRate := NewRateHertz(modelRate.Rate / 2)

	fastChanNum := fastDataBytes / (floatSize * modelRate.Rate)
	midChanNum := midDataBytes / (floatSize * midRate.Rate)
	slowChanNum := slowDataBytes / (floatSize * 16)
	slow16IChanNum := 0

	if slowChanNum > 2 {
		slowChanNum -= 2
		slow16IChanNum = 2
	}
	chanNum := fastChanNum + midChanNum + slowChanNum + slow16IChanNum

	generators := make([]Generator, 0, chanNum)
	tpGenerators := make([]Generator, 0, C.DAQ_GDS_MAX_TP_NUM)

	midChannelBoundary := fastChanNum + midChanNum
	slowChannelBoundary := midChannelBoundary + slowChanNum

	chDb := newDefaultChNumDb()
	tpDb := newDefaultChNumDb()

	for i := 0; i < fastChanNum; i++ {
		generators = append(generators, NewGPSSecondWithOffset(BasicGeneratorParams{
			BaseName: fmt.Sprintf("%s-%d", name, i),
			DcuId:    DcuId,
			DataType: dataTypeInt32,
			DataRate: modelRate,
			ChNum:    chDb.Next(),
		}, (i+DcuId)%21))
	}
	for i := fastChanNum; i < midChanNum; i++ {
		generators = append(generators, NewGPSSecondWithOffset(BasicGeneratorParams{
			BaseName: fmt.Sprintf("%s-%d", name, i),
			DcuId:    DcuId,
			DataType: dataTypeInt32,
			DataRate: midRate,
			ChNum:    chDb.Next(),
		}, (i+DcuId)%21))
	}
	for i := midChannelBoundary; i < slowChannelBoundary; i++ {

		dataType := dataTypeInt32
		if i%2 != 0 {
			dataType = dataTypeUInt32
		}
		generators = append(generators, NewGPSMod100kSecWithOffsetAndCycle(BasicGeneratorParams{
			BaseName: fmt.Sprintf("%s-%d", name, i),
			DcuId:    DcuId,
			DataType: dataType,
			DataRate: slowRateHertz,
			ChNum:    chDb.Next(),
		}, (i+DcuId)%21))
	}
	for i := slowChannelBoundary; i < chanNum; i++ {
		generators = append(generators, NewGPSMod100kSecWithOffsetAndCycle(BasicGeneratorParams{
			BaseName: fmt.Sprintf("%s-%d", name, i),
			DcuId:    DcuId,
			DataType: dataTypeInt16,
			DataRate: slowRateHertz,
			ChNum:    chDb.Next(),
		}, (i+DcuId)%21))

	}

	for i := 0; i < C.DAQ_GDS_MAX_TP_NUM; i++ {
		tpGenerators = append(tpGenerators, NewGPSMod100kSecWithOffsetAndCycle(BasicGeneratorParams{
			BaseName: fmt.Sprintf("%s-TP%d", name, i),
			DcuId:    DcuId,
			DataType: dataTypeFloat32,
			DataRate: modelRate,
			ChNum:    tpDb.Next(),
		}, (i+DcuId)%21))
	}
	nullGenerator := NewStaticGenerator(BasicGeneratorParams{
		BaseName: "null_tp_value",
		DcuId:    DcuId,
		DataType: dataTypeFloat32,
		DataRate: modelRate,
		ChNum:    0x7fffffff,
	}, 0.0)

	crcVal, err := iniManager.Add(name, DcuId, modelRate, generators, tpGenerators)
	if err != nil {
		return nil, err
	}

	return &Model{
		Name:         name,
		DcuId:        DcuId,
		ModelRate:    modelRate,
		DataRate:     dataRate,
		ConfigCrc:    crcVal,
		Status:       status,
		TPTable:      cleanTpList(tpList),
		Generators:   generators,
		TPGenerators: tpGenerators,
		NullTP:       nullGenerator,
		ShmemObj:     nil,
		TP_Shmem:     nil,
		AWG_Shmem:    nil,
		rmipc:        0,
		ShmemPrefix:  shmemPrefix,
		lastNum:      chanNum,
		lastChNum:    chDb.Next(),
	}, nil
}

func CloseModels(models []*Model) {
	for _, model := range models {
		model.close()
	}
}

func (m *Model) close() {
	if(m.AWG_Shmem != nil) {
		m.AWG_Shmem.Close()
	}
	if(m.TP_Shmem != nil) {
		m.TP_Shmem.Close()
	}
}

func (m *Model) Mutate(params MutationRequest, iniManager *IniManager) {
	curDataRate := m.DataRate

	isProtected := func(name string) bool {
		for _, cur := range params.ProtectedChannels {
			if cur == name {
				return true
			}
		}
		return false
	}

	lookup := func(name string) int {
		for i, cur := range m.Generators {
			if cur.FullChannelName() == name {
				return i
			}
		}
		return -1
	}

	removeByIndex := func(index int) bool {
		if index < 0 {
			return false
		}
		name := m.Generators[index].Name()
		if isProtected(name) {
			return false
		}
		g := m.Generators[index]
		curDataRate.Rate -= g.BytesPerSec().Rate

		m.Generators = append(m.Generators[:index], m.Generators[index+1:]...)
		return true
	}

	removeByName := func(name string) bool {
		return removeByIndex(lookup(name))
	}

	m.Status = ModelStopped

	curChans := len(m.Generators)
	count := params.RemoveCount
	if count > curChans {
		count = curChans
	}

	chDb := newChNumDb(m.lastChNum)

	// first remove specified channels
	for _, name := range params.RemoveChannels {
		if removeByName(name) {
			count--
		}
	}
	// now remove random channels ( at least try to as channels may be protected )
	for ; count > 0; count-- {
		tries := 0
		deleted := false
		for ; tries < 5 && !deleted; tries++ {
			if removeByIndex(rand.Intn(len(m.Generators))) {
				deleted = true
			}
		}
	}

	// now add channels
	count = params.AddCount
	// add specific channels
	for _, name := range params.AddChannels {
		if g, err := NewGenerator(name, params.DcuId, chDb.Next()); err == nil {
			m.Generators = append(m.Generators, g)
			count--
		}
	}
	// add random channels
	for ; count > 0; count-- {
		m.Generators = append(m.Generators, NewGPSMod100kSecWithOffsetAndCycle(BasicGeneratorParams{
			BaseName: fmt.Sprintf("%s-%d", m.Name, m.lastNum),
			DcuId:    m.DcuId,
			DataType: dataTypeInt32,
			DataRate: NewRateHertz(16),
			ChNum:    chDb.Next(),
		}, (m.lastNum+m.DcuId)%21))
		m.lastNum++
	}
	m.lastChNum = chDb.Next()

	m.ConfigCrc, _ = iniManager.Add(m.Name, m.DcuId, m.ModelRate, m.Generators, m.TPGenerators)
}

func (m *Model) OpenTPShmem() error {
	var err error
	if m.TP_Shmem != nil || m.Status == ModelStopped {
		return errors.New("model not running")
	}

	shmemName := m.ShmemPrefix + m.Name + C.SHMEM_TESTPOINT_SUFFIX
	m.TP_Shmem, err = NewShmem(shmemName, C.SHMEM_TESTPOINT_SIZE_MB)

	if err != nil {
		return err
	}

	shmem_ptr := m.TP_Shmem.Mapping()

	m.TP_cfg = (*C.struct_TESTPOINT_CFG)(unsafe.Pointer(shmem_ptr))
	return nil
}

func (m *Model) OpenAWGShmem() error {
	var err error
	if m.AWG_Shmem != nil || m.Status == ModelStopped {
		return errors.New("model not running")
	}

	shmemName := m.ShmemPrefix + m.Name + C.SHMEM_AWG_SUFFIX
	m.AWG_Shmem, err = NewShmem(shmemName, C.SHMEM_AWG_SIZE_MB)

	if err != nil {
		return err
	}

	shmem_ptr := m.AWG_Shmem.Mapping()

	m.AWG_data = (*C.struct_AWG_DATA)(unsafe.Pointer(shmem_ptr))
	return nil
}

func (m *Model) OpenRMIPC() error {
	var err error
	if m.ShmemObj != nil || m.Status == ModelStopped {
		return errors.New("model not running")
	}

	shmemName := m.ShmemPrefix + m.Name + "_daq"
	m.ShmemObj, err = NewShmem(shmemName, 64)

	if err != nil {
		return err
	}
	m.rmipc = m.ShmemObj.Mapping()

	ipc := (*C.struct_rmIpcStr)(unsafe.Pointer(m.rmipc + uintptr(C.CDS_DAQ_NET_IPC_OFFSET)))
	tpData := (*C.struct_cdsDaqNetGdsTpNum)(unsafe.Pointer(m.rmipc + uintptr(C.CDS_DAQ_NET_GDS_TP_TABLE_OFFSET)))

	ipc.cycle = ^C.uint(0)
	ipc.dcuId = C.uint(m.DcuId)
	ipc.crc = 0
	ipc.command = 0
	ipc.cmdAck = 0
	ipc.request = 0
	ipc.reqAck = 0
	ipc.status = C.uint(0xbad)
	ipc.channelCount = C.uint(len(m.Generators))
	ipc.dataBlockSize = C.uint(m.DataRatePerSec() / int(C.DAQ_NUM_DATA_BLOCKS_PER_SECOND))
	for i := 0; i < C.DAQ_NUM_DATA_BLOCKS; i++ {
		ipc.bp[i].status = C.uint(0xbad)
		ipc.bp[i].timeSec = 0
		ipc.bp[i].timeNSec = 0
		ipc.bp[i].run = 0
		ipc.bp[i].cycle = 0
		ipc.bp[i].crc = ipc.dataBlockSize
	}
	tpData.count = C.int(0)
	for i := 0; i < C.DAQ_GDS_MAX_TP_NUM; i++ {
		tpData.tpNum[i] = 0
	}
	return nil
}

func (m *Model) ActiveTP() int {
	last := -1
	for i, val := range m.TPTable {
		if val > 0 {
			last = i
		}
	}
	return last + 1
}

func (m *Model) TpDataRatePerSec() int {
	return m.ModelRate.Rate * m.ActiveTP() * int(unsafe.Sizeof(C.float(0)))
}

func (m *Model) DataRatePerSec() int {
	sum := 0
	for _, generator := range m.Generators {
		sum += generator.BytesPerSec().Rate
	}
	return sum
}

func (m *Model) CheckTestpoints() {
	testpoints := make([]C.int32_t, C.DAQ_GDS_MAX_TP_NUM)
	src, _ := int32Slice(uintptr(unsafe.Pointer(&m.TP_cfg.excitations)), C.DAQ_GDS_MAX_TP_NUM)
	copy(testpoints, src)
	sort.Slice(testpoints, func(i,j int) bool { return testpoints[i] < testpoints[j]})

	tp_table := make([]int, len(m.TPTable))
	copy(tp_table, m.TPTable)
	sort.Ints(tp_table)

	cfg_ptr := len(testpoints) - 1
	table_ptr := len(tp_table) - 1

	var to_add []int
	to_delete := make(map[int]bool)

	for (cfg_ptr >= 0 && testpoints[cfg_ptr] > 0) ||
		(table_ptr >= 0 && tp_table[table_ptr] > 0) {
		var tp, tt int
		if cfg_ptr >= 0 {
			tp = int(testpoints[cfg_ptr])
		} else {
			tp = 0
		}
		if table_ptr >= 0 {
			tt = tp_table[table_ptr]
		} else {
			tt = 0
		}
		if tp > tt {
			to_add = append(to_add, tp)
			cfg_ptr--
		} else if tt > tp {
			to_delete[tt] = true
			table_ptr--
		} else {
			// no change
			table_ptr--
			cfg_ptr--
		}
	}

	for i := 0; i < len(m.TPTable); i++ {
		if m.TPTable[i] > 0 {
			_, ok := to_delete[m.TPTable[i]]
			if ok {
				println("closing testpoint ", m.TPTable[i])
				m.TPTable[i] = 0
			}
		}
		if m.TPTable[i] == 0 && len(to_add) > 0 {

			index := -1
			for j:=0; j < 9 && index == -1; j++ {
				if src[j] == C.int(to_add[len(to_add) - 1]) {
					index = j
				}
			}

			if index >= 0 {
				m.TPGenerators[i] = NewAwgGenerator(m, index)
			}


			m.TPTable[i] = to_add[len(to_add) - 1]
			to_add = to_add[:len(to_add) - 1]
			println("opening testpoint ", m.TPTable[i])
		}
	}

	for len(m.TPTable) < cap(m.TPTable) && len(to_add) > 0 {

		index := -1
		for j:=0; j < 9 && index == -1; j++ {
			if src[j] == C.int(to_add[len(to_add) - 1]) {
				index = j
			}
		}

		if index >= 0 {
			m.TPGenerators[len(m.TPTable)] = NewAwgGenerator(m, index)
		}

		m.TPTable = append(m.TPTable, to_add[len(to_add) - 1])
		to_add = to_add[:len(to_add) - 1]
		println("opening testpoint ", m.TPTable[len(m.TPTable) - 1])
	}


}
