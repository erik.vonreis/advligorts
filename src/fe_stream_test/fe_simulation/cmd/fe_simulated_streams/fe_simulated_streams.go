package main

import (
	"flag"
	"fmt"
	"git.ligo.org/cds/advligorts/src/fe_stream_test/fe_simulation"
	"log"
)

func parseArgs() fe_simulation.InitialOptions {
	iniPath := flag.String("i", "", "Path to the ini/par file folder to use (should be a full path)")
	masterPath := flag.String("M", "master", "Path to the master file (will be overwritten)")
	mbufName := flag.String("b", "local_dc", "Name of the output shared memory buffer")
	mbufSize := flag.Int("m", 100, "Size in MB of the output shared memory buffer")
	modelSize := flag.Int("k", 700, "Default data rate of each model in kB")
	modelCount := flag.Int("R", 0, "Number of models to simulate [1-247]")
	requestedDcus := flag.String("D", "", "Simulate the given dcus (dcu,dcu,dcu,...)")
	requestedTps := flag.String("t", "", "Enable the given testpoints on the given dcu (dcu:tp#,dcu:tp#,dcu:tp#,...)")
	requestedFailures := flag.String("f", "", "Fail the given dcu's, configs written, but not started (dcu,dcu,dcu,...)")
	individualMbufs := flag.Bool("S", false, "Set if each model should write its own mbuf rmIpc structs")
	shmemPrefix := flag.String("s", "mbuf://", "Set the shmem type mbuf:// or shm://")
	adminIface := flag.String("admin", "", "Set to the host:port to open an http interface on for admin")
	useAWG := flag.Bool("a", false, "Use awgtpman to set testpoints and excitations")

	flag.Parse()

	dcuList, err1 := ParseIntList(*requestedDcus)
	tpList, err2 := ParseTPList(*requestedTps)
	failList, err3 := ParseIntList(*requestedFailures)

	if err1 != nil || err2 != nil || err3 != nil {
		log.Fatal("Improper list found in the dcu list, test point list, or fail list.")
	}
	if *modelSize < 10 || *modelSize > 3900 {
		log.Fatalf("The model data rate must between [10, 3900]")
	}
	if *mbufSize < 10 || *mbufSize > 100 {
		log.Fatal("The mbuf size must be between between [40, 100]")
	}
	if *modelCount > 0 && len(dcuList) != 0 {
		log.Fatalf("Only specify the model count if an explicit dcu list is not given")
	}
	if *modelCount > 0 {
		dcuList = getNDcus(*modelCount)
	}
	if len(dcuList) == 0 {
		log.Fatal("No dcus identified, either give a concrete dcu list or a model count")
	}

	models := make([]fe_simulation.ModelParams, 0, len(dcuList))
	for _, dcuid := range dcuList {
		tps := make([]int, 0, 0)
		status := fe_simulation.ModelRunning

		for _, curTp := range tpList {
			if curTp.DCUId == dcuid {
				tps = append(tps, int(curTp.TP))
			}
		}
		for _, failedDcu := range failList {
			if failedDcu == dcuid {
				status = fe_simulation.ModelStopped
				break
			}
		}

		model := fe_simulation.ModelParams{
			Name:      fmt.Sprintf("mod%v", dcuid),
			ModelRate: 2048,
			DataRate:  *modelSize * 1024,
			TPList:    tps,
			Status:    status,
			DCUId:     dcuid,
		}
		models = append(models, model)
	}

	opts := fe_simulation.InitialOptions{
		Models:      models,
		IniRoot:     *iniPath,
		MasterPath:  *masterPath,
		ShmemPrefix: *shmemPrefix,
		MBufName:    *mbufName,
		MBufSizeMB:  *mbufSize,
		Concentrate: !*individualMbufs,
		AdminIface:  *adminIface,
		UseAWG: *useAWG,
	}

	return opts
}

func main() {
	opts := parseArgs()

	//fmt.Println(opts)

	// 	fmt.Println("Press enter to continue...")
	// 	reader := bufio.NewReader(os.Stdin)
	// 	_, _ = reader.ReadString('\n')

	simLoop, err := fe_simulation.NewSimulationLoop(opts)
	if err != nil {
		log.Fatalf("Unable to create simulation loop %v", err)
	}
	if opts.AdminIface != "" {
		go runAdminInterface(simLoop, opts.AdminIface)
	}
	simLoop.Run()
}
