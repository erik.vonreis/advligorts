package middleware

import (
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"reflect"
	"strconv"
	"strings"
)

func parseInputJson(destValue reflect.Value, r io.Reader) error {
	input := make(map[string]interface{})
	rawData, err := ioutil.ReadAll(r)
	if err != nil {
		return err
	}
	if err = json.Unmarshal(rawData, &input); err != nil {
		return err
	}
	destType := destValue.Type()
	for i := 0; i < destValue.NumField(); i++ {
		curField := destValue.Field(i)
		curType := destType.Field(i)

		fieldName := curType.Name

		rawInputVal, ok := input[fieldName]
		if !ok {
			return errors.New("required field name missing")
		}

		inputVal := reflect.ValueOf(rawInputVal)
		inputType := inputVal.Type()

		if inputType.AssignableTo(curType.Type) {
			curField.Set(inputVal)
		} else {
			return errors.New("incompatible field types found")
		}
	}
	return nil
}

func parseInputForm(destValue reflect.Value, input url.Values) error {
	destType := destValue.Type()
	for i := 0; i < destValue.NumField(); i++ {
		curField := destValue.Field(i)
		curType := destType.Field(i)

		fieldName := curType.Name

		rawInputVal, ok := input[fieldName]
		if !ok {
			return errors.New("required field name missing")
		}
		if len(rawInputVal) != 1 {
			return errors.New("cannot handle multi value fields")
		}

		switch curField.Kind() {
		case reflect.Int:
			if val, err := strconv.ParseInt(rawInputVal[0], 10, 64); err != nil {
				return errors.New("unable to parse int value")
			} else {
				curField.SetInt(val)
			}
		case reflect.Int32:
			if val, err := strconv.ParseInt(rawInputVal[0], 10, 32); err != nil {
				return errors.New("unable to parse int value")
			} else {
				curField.SetInt(val)
			}
		case reflect.Int64:
			if val, err := strconv.ParseInt(rawInputVal[0], 10, 64); err != nil {
				return errors.New("unable to parse int value")
			} else {
				curField.SetInt(val)
			}
		case reflect.Float32:
			if val, err := strconv.ParseFloat(rawInputVal[0], 32); err != nil {
				return errors.New("unable to parse float value")
			} else {
				curField.SetFloat(val)
			}
		case reflect.Float64:
			if val, err := strconv.ParseFloat(rawInputVal[0], 64); err != nil {
				return errors.New("unable to parse float value")
			} else {
				curField.SetFloat(val)
			}
		case reflect.Bool:
			if val, err := strconv.ParseBool(rawInputVal[0]); err != nil {
				return errors.New("unable to parse bool value")
			} else {
				curField.SetBool(val)
			}
		case reflect.String:
			curField.SetString(rawInputVal[0])
		default:
			return errors.New("unsupported field type")
		}
	}
	return nil
}

func ParseInput(dest interface{}, r *http.Request) error {
	if r.Method != http.MethodPost {
		return errors.New("request type must be POST")
	}

	destValue := reflect.ValueOf(dest).Elem()
	if destValue.Kind() != reflect.Struct {
		return errors.New("dest must be a struct type")
	}
	if !destValue.CanSet() {
		return errors.New("dest must be writable")
	}
	contentType := r.Header.Get("Content-Type")
	for _, prefix := range []string{"application/json", "text/json"} {
		if strings.HasPrefix(contentType, prefix) {
			return parseInputJson(destValue, r.Body)
		}
	}

	if err := r.ParseForm(); err != nil {
		return err
	}
	return parseInputForm(destValue, r.PostForm)
}
