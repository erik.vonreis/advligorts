
#setup any rpc generation

if(APPLE)
    SET( RPCFLAGS -N -C -M -DLIGO_GDS -DGDS_NO_EPICS ${max_chnname_size})
    FIND_PROGRAM(RPCGEN_PROG rpcgen-mt)
    add_compile_definitions(
            u_int=unsigned\ int
            u_long=unsigned\ long
            u_short=unsigned\ short
    )
elseif(UNIX)
    SET( RPCFLAGS -C -N -M -DLIGO_GDS -DGDS_NO_EPICS ${max_chnname_size})
    FIND_PROGRAM(RPCGEN_PROG rpcgen)
endif()

MESSAGE("Found rpcgen at ${RPCGEN_PROG}")

SET (GDS_GEN_RPC_HDRS "")
SET (RPC_BASE gdsrsched rawgapi rtestpoint)
SET (RPC_OUTPUT_DIR ${CMAKE_BINARY_DIR}/rpc_output)

set_property(GLOBAL PROPERTY RPC_OUT_FILES "" )
set_property(GLOBAL PROPERTY RPC_TARGETS "" )

FILE(MAKE_DIRECTORY ${RPC_OUTPUT_DIR})

#    add_compile_definitions(
#	u_int=unsigned\ int
#        u_long=unsigned\ long
#        u_short=unsigned\ short
#    )

function(CREATE_RPC_TARGET basename)
    SET(OUTPATH ${RPC_OUTPUT_DIR}/${basename})
    SET(INNAME ${CMAKE_CURRENT_SOURCE_DIR}/${basename}.x)
    MESSAGE(STATUS "Generating rpc rules for ${INNAME}")
    SET(OUTFILES
            ${OUTPATH}.h
            ${OUTPATH}_svc.c
            ${OUTPATH}_xdr.c
            ${OUTPATH}_clnt.c)
    SET(intfiles
            ${OUTPATH}.h
            ${OUTPATH}_svc.c_
            ${OUTPATH}_xdr.c_
            ${OUTPATH}_clnt.c_)
    add_custom_command(OUTPUT ${OUTFILES}
            COMMAND ${CMAKE_COMMAND} -E remove -f ${intfiles}
            COMMAND ${RPCGEN_PROG} ${RPCFLAGS} -h -o ${basename}.h ${INNAME}
            COMMAND ${RPCGEN_PROG} ${RPCFLAGS} -c -o ${basename}_xdr.c_ ${INNAME}
            COMMAND ${RPCGEN_PROG} ${RPCFLAGS} -l -o ${basename}_clnt.c_ ${INNAME}
            COMMAND ${RPCGEN_PROG} ${RPCFLAGS} -m -o ${basename}_svc.c_ ${INNAME}
            COMMAND ${CMAKE_SOURCE_DIR}/src/gds/rpc/cleanrpc ${basename}
            MAIN_DEPENDENCY ${CMAKE_CURRENT_SOURCE_DIR}/${basename}.x
            WORKING_DIRECTORY ${RPC_OUTPUT_DIR}
            )

    SET(libname ${basename}_rpc)
    add_library(${libname} OBJECT
            ${OUTFILES})
    if(APPLE)
        target_compile_options(${libname} PUBLIC -std=gnu99)
        target_compile_definitions(${libname} PUBLIC
                u_int=unsigned\ int
                u_long=unsigned\ long
                u_short=unsigned\ short
                )
    endif()
    target_include_directories(${libname} PUBLIC
            ${DTT_INCLUDES})
    get_property(tmp GLOBAL PROPERTY RPC_OUT_FILES)
    set_property(GLOBAL PROPERTY RPC_OUT_FILES ${tmp} ${OUTFILES} )

    get_property(tmp GLOBAL PROPERTY RPC_TARGETS)
    set_property(GLOBAL PROPERTY RPC_TARGETS ${tmp} ${libname})

endfunction()

function(CREATE_RPC_TARGET_NO_SVC basename)
    SET(OUTPATH ${RPC_OUTPUT_DIR}/${basename})
    SET(INNAME ${CMAKE_CURRENT_SOURCE_DIR}/${basename}.x)
    MESSAGE(STATUS "Generating rpc rules for ${INNAME}")
    SET(OUTFILES
            ${OUTPATH}.h
            #${OUTPATH}_svc.c
            ${OUTPATH}_xdr.c
            ${OUTPATH}_clnt.c)
    SET(intfiles
            ${OUTPATH}.h
            ${OUTPATH}_svc.c_
            ${OUTPATH}_xdr.c_
            ${OUTPATH}_clnt.c_)
    add_custom_command(OUTPUT ${OUTFILES}
            COMMAND ${CMAKE_COMMAND} -E remove -f ${intfiles}
            COMMAND ${RPCGEN_PROG} ${RPCFLAGS} -h -o ${basename}.h ${INNAME}
            COMMAND ${RPCGEN_PROG} ${RPCFLAGS} -c -o ${basename}_xdr.c_ ${INNAME}
            COMMAND ${RPCGEN_PROG} ${RPCFLAGS} -l -o ${basename}_clnt.c_ ${INNAME}
            COMMAND ${RPCGEN_PROG} ${RPCFLAGS} -m -o ${basename}_svc.c_ ${INNAME}
            COMMAND ${CMAKE_SOURCE_DIR}/scripts/cleanrpc ${basename}
            MAIN_DEPENDENCY ${CMAKE_CURRENT_SOURCE_DIR}/${basename}.x
            WORKING_DIRECTORY ${RPC_OUTPUT_DIR}
            )

    SET(libname ${basename}_rpc)
    add_library(${libname} OBJECT
            ${OUTFILES})
    if(APPLE)
        target_compile_options(${libname} PUBLIC -std=gnu99)
        target_compile_definitions(${libname} PUBLIC
                u_int=unsigned\ int
                u_long=unsigned\ long
                u_short=unsigned\ short
                )
    endif()
    target_include_directories(${libname} PUBLIC
            ${DTT_INCLUDES})
    get_property(tmp GLOBAL PROPERTY RPC_OUT_FILES)
    set_property(GLOBAL PROPERTY RPC_OUT_FILES ${tmp} ${OUTFILES} )

    get_property(tmp GLOBAL PROPERTY RPC_TARGETS)
    set_property(GLOBAL PROPERTY RPC_TARGETS ${tmp} ${libname})

endfunction(CREATE_RPC_TARGET_NO_SVC)

add_subdirectory(awgtpman)
add_subdirectory(rpc)