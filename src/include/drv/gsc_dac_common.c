/// @file gsc_dac_common.c
/// @brief File contains routines common to all DAC modules
#include "gsc_dac_common.h"

void 
gscdacfirstlast(int card, int total_cards, int* first, int* last)
{ 
    if ( card == GSAI_ALL_CARDS )
    {
        *first = 0;
        *last = total_cards;
    }
    else
    {
        *first = card;
        *last = card + 1;
    }
}
//***********************************************************************
/// \brief // This routine will enable or disable external clocking on one
///< or all DAC cards.
/// @param[in] card to enable/disable
/// @param[in] enable or disable flag
//***********************************************************************
void
gscEnableDacModule( CDS_HARDWARE* pHardware, int card, int enable)
{
    int ii;
    int fc, lc;

    gscdacfirstlast(card,pHardware->dacCount,&fc,&lc );

    for ( ii = fc; ii < lc; ii++ )
    {
        switch(pHardware->dacType[ii])
        {
        case GSC_16AO16:
            if(enable)
            {
                _dacPtr[ ii ]->BOR |= GSAO_ENABLE_CLK ;
            } else {
                _dacPtr[ ii ]->BOR &= ~( GSAO_ENABLE_CLK ) ;
            }

            break;
        case GSC_18AO8:
            if(enable)
            {
                _dacPtr[ ii ]->OUTPUT_CONFIG |= GSAO_18BIT_EXT_CLOCK_SRC;
                _dacPtr[ ii ]->BUF_OUTPUT_OPS |= GSAO_18BIT_ENABLE_CLOCK;
            } else {
                _dacPtr[ ii ]->OUTPUT_CONFIG &= ~( GSAO_18BIT_EXT_CLOCK_SRC );
                _dacPtr[ ii ]->BUF_OUTPUT_OPS &= ~( GSAO_18BIT_ENABLE_CLOCK );
            }
            break;
        case GSC_20AO8:
            if(enable)
            {
                _dacPtr[ ii ]->OUTPUT_CONFIG |= GSAO_20BIT_EXT_CLOCK_SRC;
                _dacPtr[ ii ]->BUF_OUTPUT_OPS |= GSAO_20BIT_ENABLE_CLOCK;
            } else {
                _dacPtr[ ii ]->OUTPUT_CONFIG &= ~( GSAO_20BIT_EXT_CLOCK_SRC );
                _dacPtr[ ii ]->BUF_OUTPUT_OPS &= ~( GSAO_20BIT_ENABLE_CLOCK );
            }
            break;
        default:
            break;
        }
    }
    udelay(MAX_UDELAY);
}


//***********************************************************************
/// \brief This routine is used to test if a DAC FIFO is empty or not empty.
/// @param[in] card ADC card to check
/// @param[in] checktype check type ie empty or !empty
/// @return -1 if error or 0 if ok
//***********************************************************************
int
gscDacFifoCheck( CDS_HARDWARE* pHardware, int card , int checktype)
{
    int retval = -1;
    u32 status = 0;

    if(pHardware->dacType[ card ] == GSC_16AO16)
    {
        status = gsc16ao16CheckDacBuffer( card );
        status &= 1;
        if(status && checktype == DAC_FIFO_EMPTY_TEST) retval = 0;
        if(!status && checktype == DAC_FIFO_NOT_EMPTY_TEST) retval = 0;
    } else {
        status =  _dacPtr[ card ]->OUT_BUF_SIZE;
        if(!status && checktype == DAC_FIFO_EMPTY_TEST) retval = 0;
        if(status && checktype == DAC_FIFO_NOT_EMPTY_TEST) retval = 0;
    }
    
    return retval;
}

//***********************************************************************
/// \brief This routine sends a DAC FIFO clear command to requested DAC module.
//***********************************************************************
void
gscDacFifoClear( CDS_HARDWARE* pHardware, int card )
{
    int fc = 0;
    int lc = 0;
    int ii = 0;

    gscdacfirstlast(card,pHardware->dacCount,&fc,&lc );

    for ( ii = fc; ii < lc; ii++ )
    {
    if(pHardware->dacType[ ii ] == GSC_16AO16)
    {
        _dacPtr[ ii ]->BOR |= DAC_FIFO_CLEAR;
    } else {
        _dacPtr[ ii ]->BUF_OUTPUT_OPS |= DAC_FIFO_CLEAR;
    }
}
}

//***********************************************************************
/// \brief This routine sends a DAC soft clock command to requested DAC module.
//***********************************************************************
void
gscDacSoftClock( CDS_HARDWARE* pHardware, int card )
{
    int fc = 0;
    int lc = 0;
    int ii = 0;

    gscdacfirstlast(card,pHardware->dacCount,&fc,&lc );

    for ( ii = fc; ii < lc; ii++ )
    {
        switch( pHardware->dacType[ ii ] )
        {
        case GSC_16AO16:
            _dacPtr[ ii ]->BOR =  GSAO_BOR_SW_CLOCK ;
            break;
        case GSC_18AO8:
        case GSC_20AO8:
            _dacPtr[ ii ]->BUF_OUTPUT_OPS = GSAO_18BIT_BOO_SW_CLOCK;
            break;
        default:
            break;

        }
    }
}
