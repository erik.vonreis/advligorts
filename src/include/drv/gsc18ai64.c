///	\file gsc18ai64.c
///	\brief File contains the initialization routine and various register
/// read/write
///<		operations for the General Standards 18bit, 64 channel ADC
///< modules. \n

#include "gsc_adc_common.h"
#include "gsc18ai64.h"

// *****************************************************************************
/// \brief Routine to initialize GSC 18bit, 64 channel ADC modules
///     @param[in,out] *pHardware Pointer to global data structure for storing
///     I/O
///<            register mapping information.
///     @param[in] *adcdev PCI address information passed by the mapping code in
///     map.c
///	@return Status from board enable command.
// *****************************************************************************
int
gsc18ai64Init( CDS_HARDWARE* pHardware, struct pci_dev* adcdev )
{
    static unsigned int pci_io_addr; /// @param pci_io_addr Bus address of PCI
                                     /// card I/O register.
    int devNum; /// @param devNum Index into CDS_HARDWARE struct for adding
                /// board info.
    char* _adc_add; /// @param *_adc_add ADC register address space
    int   pedStatus; /// @param pedStatus Status return from call to enable
                   /// device.
    int                   autocal = 0;
    volatile GSA_ADC_REG* adc18Ptr;
    volatile u32          card_config = 0;

    /// Get index into CDS_HARDWARE struct based on total number of ADC cards
    /// found by mapping routine in map.c
    devNum = pHardware->adcCount;
    /// Enable the module.
    pedStatus = pci_enable_device( adcdev );
    /// Enable device to be DMA master.
    pci_set_master( adcdev );
    /// Get the PLX chip address
    pedStatus = pci_read_config_dword( adcdev, PCI_BASE_ADDRESS_0, &pci_io_addr );
    if(pedStatus != 0)
        return -1;
    printk( "pci0 = 0x%x\n", pci_io_addr );
    /// Map module DMA space directly to computer memory space.
    _adc_add = ioremap_nocache( (unsigned long)pci_io_addr, 0x200 );
    /// Map the module DMA control registers via PLX chip registers
    adcDma[ devNum ] = (PLX_9056_DMA*)_adc_add;

    /// Get the ADC register address
    pedStatus = pci_read_config_dword( adcdev, PCI_BASE_ADDRESS_2, &pci_io_addr );
    if(pedStatus != 0)
        return -1;
    printk( "pci2 = 0x%x\n", pci_io_addr );
    /// Map the module control register so local memory space.
    _adc_add = ioremap_nocache( (unsigned long)pci_io_addr, 0x200 );
    printk( "ADC I/O address=0x%x  0x%lx\n", pci_io_addr, (long)_adc_add );
    /// Set global ptr to control register memory space.
    adc18Ptr = (GSA_ADC_REG*)_adc_add;
    _adcPtr[ devNum ] = (GSA_ADC_REG*)_adc_add;

    printk( "BCR = 0x%x\n", _adcPtr[ devNum ]->BCR );
    /// Reset the ADC board
    adc18Ptr->BCR |= GSA7_RESET;
    do
    {
    } while ( ( adc18Ptr->BCR & GSA7_RESET ) != 0 );

    /// Write in a sync word
    adc18Ptr->SMUW = 0x0000;
    adc18Ptr->SMLW = 0x0000;

    /// Set ADC to 64 channel = 32 differential channels
    adc18Ptr->BCR |= ( GSA7_FULL_DIFFERENTIAL );

    /// Set sample rate close to 16384Hz
    /// Unit runs with external clock, so this probably not necessary
    adc18Ptr->RAG =
        (unsigned int)( GSC18AI64_OSC_FREQ / ( UNDERSAMPLE * IOP_IO_RATE ) );
    printk( "RAG = 0x%x\n", adc18Ptr->RAG );
    printk( "BCR = 0x%x\n", adc18Ptr->BCR );
    adc18Ptr->RAG &= ~( GSA7_SAMPLE_START );
    /// Initiate board calibration
    adc18Ptr->BCR |= GSA7_AUTO_CAL;
    /// Wait for internal calibration to complete.
    do
    {
        autocal++;
        udelay( 100 );
    } while ( ( adc18Ptr->BCR & GSA7_AUTO_CAL ) != 0 );
    if ( ( adc18Ptr->BCR & GSA7_AUTO_CAL_PASS ) == 0 )
    {
        printk( "ADC AUTOCAL FAIL %d\n", autocal );
        autocal = 0;
    }
    else
    {
        printk( "ADC AUTOCAL PASS %d\n", autocal );
        autocal = GSA7_AUTO_CAL_PASS;
    }
    adc18Ptr->IDBC = ( GSAI_CLEAR_BUFFER | GSAI_THRESHOLD );

    // Need to read the production configuration register for
    // number of channels and clock pin assignment
    card_config = adc18Ptr->ASSC;
    // If card only has 32 channels OR card does not have clock input on pin 33
    if ( ( ( card_config & GSA7_IS_32_CHANNEL_CARD ) > 0 ) ||
         ( ( card_config & GSA7_PIN33_IS_CLK ) == 0 ) )
    {
        pHardware->adcChannels[ devNum ] = 16;
        _adcPtr[ devNum ]->SSC =
            ( GSA7_32_CHANNEL | GSA7_EXTERNAL_SYNC | GSA7_ENABLE_CLK_PIN_38 );
    }
    else
    {
        pHardware->adcChannels[ devNum ] = 32;
        _adcPtr[ devNum ]->SSC = ( GSA7_64_CHANNEL | GSA7_EXTERNAL_SYNC );
    }

    // print diags to dmesg
    printk( "750K SSC = 0x%x\n", adc18Ptr->SSC );
    printk( "750K IDBC = 0x%x\n", adc18Ptr->IDBC );
    printk( "750K PCR = 0x%x \n", adc18Ptr->ASSC );

    /// Fill in CDS_HARDWARE structure with ADC information.
    pHardware->pci_adc[ devNum ] =
        (long)pci_alloc_consistent( adcdev, 0x2000, &adc_dma_handle[ devNum ] );
    pHardware->adcType[ devNum ] = GSC_18AI64SSC;
    pHardware->adcInstance[ devNum ] = pHardware->card_count[ GSC_18AI64SSC ];
    pHardware->card_count[ GSC_18AI64SSC ] ++;
    pHardware->adcConfig[ devNum ] = adc18Ptr->ASSC;
    pHardware->adcConfig[ devNum ] |= autocal;
    gsc18ai64ReadRegisters( pHardware, devNum );
    pHardware->adcCount++;
    /// Return board enable status.
    return ( pedStatus );
}

// *****************************************************************************
/// \brief Function clears ADC buffer and starts acquisition via external clock.
///< Also sets up ADC for Demand DMA mode and set GO bit in DMA Mode Register.
///< NOTE: In normal operation, this code should only be called while the clocks
///< from the timing receiver are turned OFF ie during initialization process.
///	@param[in] adcCount Total number of ADC modules to start DMA.
// *****************************************************************************
int
gsc18ai64Clock( CDS_HARDWARE* pHardware, int modnum )
{
    volatile GSA_ADC_REG* adc18Ptr;

    adc18Ptr = (volatile GSA_ADC_REG*)_adcPtr[ modnum ];
    adc18Ptr->IDBC = ( GSAI_CLEAR_BUFFER | GSAI_THRESHOLD );
    /// Enable sync via external clock input.
    adc18Ptr->SSC |= GSA7_CLOCK_ENABLE;
}

// *****************************************************************************
/// \brief Test routine which dumps card register information.
// *****************************************************************************
void
gsc18ai64ReadRegisters( CDS_HARDWARE* pHardware, int modnum )
{
    unsigned int          ii;
    volatile GSA_ADC_REG* adc18Ptr;
    unsigned int          bcr_value;

    adc18Ptr = (volatile GSA_ADC_REG*)_adcPtr[ modnum ];
    bcr_value = adc18Ptr->BCR;
    ii = bcr_value & 7;
    printk( "750KHz BCR AIM = %d\n", ii );
    ii = ( bcr_value >> 3 );
    printk( "750KHz BCR Unipolar = %d\n", ( ii & 1 ) );
    ii = ( bcr_value >> 4 );
    printk( "750KHz BCR Range = %d\n", ( ii & 3 ) );
    ii = ( bcr_value >> 6 );
    printk( "750KHz BCR Offset Binary = %d\n", ( ii & 1 ) );
    ii = ( bcr_value >> 7 );
    printk( "750KHz BCR 16 Bit = %d\n", ( ii & 1 ) );
    ii = ( bcr_value >> 8 );
    printk( "750KHz BCR Differential Processing  = %d\n", ( ii & 3 ) );
    ii = ( bcr_value >> 10 );
    printk( "750KHz BCR Data on Hold = %d\n", ( ii & 1 ) );
    ii = ( bcr_value >> 11 );
    printk( "750KHz BCR Disable Scan Marker = %d\n", ( ii & 1 ) );
    ii = ( bcr_value >> 12 );
    printk( "750KHz BCR Burst Trigger = %d\n", ( ii & 1 ) );
    ii = ( bcr_value >> 13 );
    printk( "750KHz BCR Autocal = %d\n", ( ii & 1 ) );
    ii = ( bcr_value >> 14 );
    printk( "750KHz BCR Autocal Pass = %d\n", ( ii & 1 ) );
    ii = ( bcr_value >> 15 );
    printk( "750KHz BCR Initialize = %d\n", ( ii & 1 ) );
    ii = ( bcr_value >> 16 );
    printk( "750KHz BCR Buffer Underflow = %d\n", ( ii & 1 ) );
    ii = ( bcr_value >> 17 );
    printk( "750KHz BCR Buffer Overflow = %d\n", ( ii & 1 ) );
    ii = ( bcr_value >> 18 );
    printk( "750KHz BCR Data Packing = %d\n", ( ii & 1 ) );
    ii = ( bcr_value >> 19 );
    printk( "750KHz BCR Input Clock = %d\n", ( ii & 1 ) );
}

// *****************************************************************************
/// \brief Function stops ADC acquisition by removing the clocking signal.
// *****************************************************************************
void
gsc18ai64AdcStop( int modNum )
{

    _adcPtr[ modNum ]->BCR &= ~( GSA7_ENABLE_X_SYNC );
}

