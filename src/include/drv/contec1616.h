#define C_DIO_1616L_PE  0x8632

int contec1616Init( CDS_HARDWARE* , struct pci_dev* );
unsigned int contec1616WriteOutputRegister( CDS_HARDWARE* , int , unsigned int );
unsigned int contec1616ReadOutputRegister( CDS_HARDWARE* , int );
unsigned int contec1616ReadInputRegister( CDS_HARDWARE* , int );
void start_tds_clocks ( int );
void stop_tds_clocks ( int );


