#ifndef CDS_HARDWARE_H
#define CDS_HARDWARE_H
/*----------------------------------------------------------------------------- */
/*                                                                      	*/
/*                      -------------------                             	*/
/*                                                                      	*/
/*                             LIGO                                     	*/
/*                                                                      	*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.      	*/
/*                                                                      	*/
/*                     (C) The LIGO Project, 2005.                      	*/
/*                                                                      	*/
/*                                                                      	*/
/* File: cdsHardware.h								*/
/* Description:									*/
/*	Standard header files describing all PCI hardware used in CDS		*/
/*	front end control systems.						*/
/*                                                                      	*/
/* California Institute of Technology                                   	*/
/* LIGO Project MS 18-34                                                	*/
/* Pasadena CA 91125                                                    	*/
/*                                                                      	*/
/* Massachusetts Institute of Technology                                	*/
/* LIGO Project MS 20B-145                                              	*/
/* Cambridge MA 01239                                                   	*/
/*                                                                      	*/
/*----------------------------------------------------------------------------- */


/* Define maximum number of each PCI module supported.				*/
#define MAX_IOC		        2	
#define MAX_ADC_MODULES		12	
#define MAX_ADC_CHN_PER_MOD	32	
#define MAX_DAC_MODULES		12
#define MAX_DAC_CHN_PER_MOD	16	
#define MAX_DIO_MODULES		16
#define MAX_RFM_MODULES		2
#define MAX_VME_BRIDGES		4

// Supported IO card vendor IDs
// Vendor ID for ACCESS DIO Modules
#define ACC_VID		0x494F
// Vendor ID for Contec modules
#define CONTEC_VID	0x1221

// Each supported IO card must have a unique ID number,
// as defined below. 
#define GSC_16AI64SSA	0
#define GSC_18AISS6C	1
#define GSC_16AO16		2
#define GSC_20AO8       3
#define CON_32DO		4
#define ACS_16DIO		5
#define ACS_8DIO		6
#define GSC_18AI32SSC1M		7
#define GSC_18AO8		8
#define ACS_24DIO		9
#define CON_1616DIO		10
#define CON_6464DIO		11
#define CDO64			12
#define CDI64			13
#define LPTC			14
#define GSC_18AI64SSC	15
#define DUMMY_CARD		30

/* Cards configuration */
typedef struct CDS_CARDS {
	int type;
	int instance;
} CDS_CARDS;

/* Remote IPC nodes configuration */
typedef struct CDS_REMOTE_NODES {
	char nodename[64]; /* Host name */
	int port;	   /* port number; we can have multiple FEs on same host */
} CDS_REMOTE_NODES;


#define IO_MEMORY_SLOTS		4096
#define IO_MEMORY_SLOT_VALS	32
#define MAX_IO_MODULES		26
#define OVERFLOW_LIMIT_16BIT	32760
#define OVERFLOW_LIMIT_18BIT	131060
#define OVERFLOW_LIMIT_20BIT    512240
#define OVERFLOW_CNTR_LIMIT	0x1000000
#define MAX_ADC_WAIT		2000000		// Max time (usec) to wait for ADC data transfer in iop app
#define MAX_ADC_WAIT_CARD_0	(29 * (UNDERSAMPLE / ADC_MEMCPY_RATE))		// Max time (usec) to wait for 1st ADC card data ready
#define MAX_ADC_WAIT_C0_32K	36		// Max time (usec) to wait for 1st ADC card data ready on 32K IOP
#define MAX_ADC_WAIT_CARD_S	7 		// Max time (usec) to wait for remaining ADC card data ready
#define MAX_ADC_WAIT_ERR_SEC	3 		// Max number of times ADC time > WAIT per sec before alarm set.
#define MAX_ADC_WAIT_CONTROL	100000		// Max time (usec) to wait for ADC data transfer in control app
#define MAX_ADC_WAIT_CYCLE	17
#define DUMMY_ADC_VAL		0xf000000	// Dummy value for test last ADC channel has arrived
#define ADC_1ST_CHAN_MARKER	0xf0000		// Only first ADC channel should have upper bits set as first chan marker.

// If 1pps signal used for startup sync, then 1pps signal should
// be on last channel of first ADC card.
#ifdef TEST_1PPS
// On x2ats test system, 1pps is on 3rd ADC card
#define ADC_ONEPPS_BRD		2
#else
#define ADC_ONEPPS_BRD		0
#endif

// With standard LIGO timing, ADC duotone signal should
// appear on last channel of first ADC and DAC duotone
// on next to last channel of first ADC
#define ADC_DUOTONE_BRD		0
#define ADC_DUOTONE_CHAN	31
#define DAC_DUOTONE_CHAN	30

// Number of cycles after start before writing DAC ouputs
// Put in to save time on first few cycles as code runs
// slower at start
#define DAC_START_CYCLE     4 

#define ADC_BUS_DELAY		1
#define ADC_SHORT_CYCLE		2
#define ADC_MAPPED  		1
#define ADC_CHAN_HOP  		2
#define ADC_OVERFLOW  		4
#define ADC_CAL_PASS  		8
#define DAC_CAL_PASS  		0x100000
#define DAC_ACR_MASK  		0x01ffff
#define ADC_RD_TIME  		16

// Following are bit defs for display of slot status
// on IOP IO Status screen
#define CARD_MAPPED         1
#define CARD_AUTOCAL        2
#define CARD_OVERRANGE      4
#define CARD_TIMING         8
#define CARD_ADC_CHOP      16
#define CARD_DAC_WD        16
#define CARD_DAC_AIC       32
#define CARD_ADC_AIC       32
#define CARD_DAC_EMPTY     64
#define CARD_DAC_HIQTR     128
#define CARD_DAC_FULL      256


typedef struct MEM_DATA_BLOCK{
	int timeSec;
	int cycle;
	int data[32];
}MEM_DATA_BLOCK;

// Interface structure between kernel space IOP and control models
typedef struct IO_MEM_DATA{
	int gpsSecond;
	int totalCards;
	int adcCount;
	int dacCount;
	int bioCount;
	int model[MAX_IO_MODULES];
	int ipc[MAX_IO_MODULES];
	int rfmCount;
	long pci_rfm[MAX_RFM_MODULES];	/* Remapped addresses of RFM modules	*/
	long pci_rfm_dma[MAX_RFM_MODULES];	/* Remapped addresses of RFM modules	*/
    int dolphinCount;
	volatile unsigned long *dolphinRead[4]; /* read and write Dolphin memory */
	volatile unsigned long *dolphinWrite[4]; /* read and write Dolphin memory */
	MEM_DATA_BLOCK iodata[MAX_IO_MODULES][IO_MEMORY_SLOTS];
	// Combined DAC channels map; used to check on control app DAC channel allocations
	unsigned int dacOutUsed[MAX_DAC_MODULES][16];
	unsigned int ipcDetect[2][8];
	int card[MAX_IO_MODULES];
    int mem_data_rate;;
}IO_MEM_DATA;

// Interface structure between user space IOP and user space control models
typedef struct IO_MEM_DATA_IOP{
	int gpsSecond;
	int cycleNum;
	int totalCards;
	int adcCount;
	int dacCount;
	int bioCount;
	int model[MAX_IO_MODULES];
	int ipc[MAX_IO_MODULES];
	int rfmCount;
	long pci_rfm[MAX_RFM_MODULES];	/* Remapped addresses of RFM modules	*/
	long pci_rfm_dma[MAX_RFM_MODULES];	/* Remapped addresses of RFM modules	*/
        int dolphinCount;
	volatile unsigned long *dolphinRead[4]; /* read and write Dolphin memory */
	volatile unsigned long *dolphinWrite[4]; /* read and write Dolphin memory */
	MEM_DATA_BLOCK iodata[6][65536];
	// Combined DAC channels map; used to check on control app DAC channel allocations
	unsigned int dacOutUsed[MAX_DAC_MODULES][16];
	unsigned int ipcDetect[2][8];
}IO_MEM_DATA_IOP;


// Timing control register definitions for use with Contec1616 control of timing receiver.

#define TDS_STOP_CLOCKS			0x3700000
#define TDS_START_ADC_NEG_DAC_POS	0x7700000
#define TDS_START_ADC_NEG_DAC_NEG	0x7f00000
#define TDS_NO_ADC_DUOTONE		  0x10000
#define TDS_NO_DAC_DUOTONE		  0x20000

/* Offset of the IO_MEM_DATA structure in the IPC shared memory */
#define IO_MEM_DATA_OFFSET 0x4000


/* Standard structure to maintain PCI module information.			*/
typedef struct CDS_HARDWARE{
	int dacCount;			/* Number of DAC modules found 		*/
	long pci_dac[MAX_DAC_MODULES];	/* Remapped addresses of DAC modules	*/
	int dacType[MAX_DAC_MODULES];
	int dacInstance[MAX_DAC_MODULES];
	int dacSlot[MAX_DAC_MODULES];
	int dacConfig[MAX_DAC_MODULES];
	int dacMap[MAX_DAC_MODULES];
    int dacAcr[MAX_DAC_MODULES];
	int adcCount;			/* Number of ADC modules found		*/
	long pci_adc[MAX_ADC_MODULES];	/* Remapped addresses of ADC modules	*/
	int adcType[MAX_ADC_MODULES];
	int adcInstance[MAX_ADC_MODULES];
	int adcSlot[MAX_ADC_MODULES];
    int adcChannels[MAX_ADC_MODULES];
	int adcConfig[MAX_ADC_MODULES];
	int adcMap[MAX_ADC_MODULES];
	int doCount;			/* Number of DIO modules found		*/
	unsigned short pci_do[MAX_DIO_MODULES];	/* io registers of DIO	*/
	int doType[MAX_DIO_MODULES];
	int doInstance[MAX_DIO_MODULES];
	int dioCount;			/* Number of DIO modules found		*/
	unsigned short pci_dio[MAX_DIO_MODULES];	/* io registers of DIO	*/
	unsigned short pci_iiro_dio[MAX_DIO_MODULES];	/* io regs of IIRO mods */
	unsigned short pci_iiro_dio1[MAX_DIO_MODULES];	/* io regs of IIRO-16 mods */
	unsigned short pci_cdo_dio1[MAX_DIO_MODULES];	/* io regs of Contec 32BO mods */
	int rfmCount;			/* Number of RFM modules found		*/
	long pci_rfm[MAX_RFM_MODULES];	/* Remapped addresses of RFM modules	*/
	long pci_rfm_dma[MAX_RFM_MODULES];	/* Remapped addresses of RFM modules	*/
	int rfmConfig[MAX_RFM_MODULES];
	int rfmType[MAX_RFM_MODULES];
	unsigned char *buf;
	volatile unsigned int *gps;	/* GPS card */
	unsigned int gpsType;
	int gpsOffset;
	int dolphinCount;		/* the number of Dolphin cards we have  on the system */
	volatile unsigned long *dolphinRead[4]; /* read and write Dolphin memory */
	volatile unsigned long *dolphinWrite[4]; /* read and write Dolphin memory */
	volatile unsigned int *lptc[MAX_IOC];	/* LIGO PCIe Timing card */
	unsigned int ioc_config[MAX_ADC_MODULES];
	unsigned int ioc_instance[MAX_ADC_MODULES];
	unsigned int ioc_cards;
	int card_count[MAX_IO_MODULES]; // Array to count cards per IO card type

	/* Variables controlling cards usage */
	int cards;			/* Sizeof array below */
	CDS_CARDS *cards_used;		/* Cards configuration */
}CDS_HARDWARE;

#endif
