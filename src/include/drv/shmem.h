#ifndef LIGO_DRIVER_SHMEM_H
#define LIGO_DRIVER_SHMEM_H

#ifdef __cplusplus

#include <string>

extern "C" {
#endif

extern volatile void* findSharedMemory( char* sys_name );
extern volatile void* findSharedMemorySize( char* sys_name, int size );

extern int shmem_format_name( char* dest, const char* src, size_t n );
// extern volatile void* shmem_open_segment(const char *sys_name, size_t
// req_size);

#define SHMEM_MBUF_PREFIX "mbuf://"
#define SHMEM_POSIX_PREFIX "shm://"

typedef void* shmem_handle;

/**
 * @brief given a name mbuf://name, shm://name, name, ... return the start of
 * name
 * @param input input name
 * @param prefix (optional) gets a pointer to a copy of the prefix type
 * @return The portion after any prefix value, or name if no prefix found.
 */
extern const char* shmem_name_parse( const char* input, const char** prefix );

/**
 * @brief Open a shared memory segment
 * @param name name of the segment
 * @param size_mb size in megabytes 2**20
 * @return a handle != 0 on success
 */
extern shmem_handle shmem_open( const char* name, size_t size_mb );
/**
 * @brief wrap an existing block of memory, typically for part of
 * testing/debugging
 * @param data the data to wrap
 * @param size_mb size of *data in megabytes 2**20
 * @param cleanup callback to cleanup/deallocate data when shmem_close called
 * @return a handle != 0
 */
extern shmem_handle shmem_wrap( volatile void* data,
                                size_t         size_mb,
                                void ( *cleanup )( volatile void* ) );
/**
 * @brief Close a shared memory handle
 * @param handle the handle
 * @note safe to call with a null handle
 */
extern void shmem_close( shmem_handle handle );
/**
 * @brief Get the address of the memory mapping
 * @param handle the handle
 * @return null pointer or the mapped address
 * @note safe to call with a null handle
 */
extern volatile void* shmem_mapping( shmem_handle handle );

/**
 * @brief Return the size in MB that the buffer was opened as
 * @param handle the shared memory handle
 * @return The size in MB
 */
extern size_t shmem_size_mb( shmem_handle handle );

extern shmem_handle shmem_wrap_memory_arbitrary(
    volatile void* data, size_t size, void ( *cleanup )( volatile void* ) );

#ifdef __cplusplus
}

namespace shmem
{
    /**
     * @brief a simple RAII wrapper around the shmem_... functions
     */
    class shmem
    {
    public:
        shmem( ) : handle_{ nullptr }
        {
        }
        shmem( const std::string& name, size_t size_mb )
            : handle_{ shmem_open( name.c_str(), size_mb ) }
        {
        }
        shmem( const char* name, size_t size_mb )
            : handle_{ shmem_open( name, size_mb ) }
        {
        }
        shmem( volatile void* data,
               size_t         size_in_bytes,
               void ( *cleanup )( volatile void* ) )
            : handle_{ shmem_wrap_memory_arbitrary(
                  data, size_in_bytes, cleanup ) }
        {
        }
        shmem( shmem&& other ) noexcept : handle_{ other.handle_ }
        {
            other.handle_ = nullptr;
        }
        shmem( const shmem& ) = delete;

        shmem&
        operator=( shmem&& other ) noexcept
        {
            if ( &other != this )
            {
                shmem_close( handle_ );
                handle_ = other.handle_;
                other.handle_ = nullptr;
            }
            return *this;
        }
        shmem& operator=( const shmem& ) = delete;

        ~shmem( )
        {
            shmem_close( handle_ );
        }

        template < typename T >
        volatile T*
        mapping( )
        {
            return reinterpret_cast< volatile T* >( shmem_mapping( handle_ ) );
        }

        size_t
        size() const noexcept
        {
            return shmem_size_mb( handle_ ) * 1024*1024;
        }

        size_t
        size_mb() const noexcept
        {
            return shmem_size_mb( handle_ );
        }

    private:
        shmem_handle handle_;
    };

    inline void
    parse_name( const std::string& input,
                std::string&       prefix,
                std::string&       name )
    {
        if ( input.empty( ) )
        {
            prefix = "";
            name = "";
            return;
        }
        const char* prefix_str = nullptr;
        const char* parsed_name =
            shmem_name_parse( input.c_str( ), &prefix_str );

        name = parsed_name;
        prefix = prefix_str;
    }
} // namespace shmem

#endif

#endif /* LIGO_DRIVER_SHMEM_H */