/// 	\file ligoPcieTiming64.c

#include "ligoPcieTiming.h"

static int
lptcInit( CDS_HARDWARE* pCds, struct pci_dev* lptcdev )
{
    static unsigned int pci_io_addr;
    int                 status = 0;
    char*               addr;
    LPTC_REGISTER*      lptcPtr;
    LPTC_OBF_REGISTER*  lptcObf;
    unsigned int        usec, sec;
    unsigned int        tsec;
    unsigned int        regval;
    int                 ii;
    int                 card = 0;
    u64 gpstime;

    card = pCds->card_count[ LPTC ];
    status = pci_enable_device( lptcdev );
    printk( "Xilinx enabled status = %d\n", status );
    pci_read_config_dword( lptcdev, PCI_BASE_ADDRESS_0, &pci_io_addr );
    pci_io_addr &= 0xfffffff0;
    addr = (char*)ioremap_nocache( (unsigned long)pci_io_addr, 0x2000 );
    printk( "Xilinx mapped  = 0x%x   0x%p\n", pci_io_addr, addr );
    pCds->lptc[ card ] = (unsigned int*)addr;

    if ( card == 0 )
    {
        pCds->gps = (unsigned int*)addr;
        pCds->gpsType = LIGO_RCVR;
    }

    lptcPtr = (LPTC_REGISTER*)addr;

    gpstime = lptcPtr->gps_time;
    usec = (gpstime & 0xffffffff) * LPTC_USEC_CONVERT;
    sec = (gpstime >> 32) & 0xffffffff;
    printk( "Xilinx time1  = %u   %u\n", sec, usec );
    udelay(1000);
    gpstime = lptcPtr->gps_time;
    usec = (gpstime & 0xffffffff) * LPTC_USEC_CONVERT;
    sec = (gpstime >> 32) & 0xffffffff;
    printk( "Xilinx time2  = %u   %u\n", sec, usec );

    printk( "Xilinx status  = 0x%x  \n", lptcPtr->status );
    printk( "Xilinx sw revision  = 0x%x  \n", lptcPtr->revision );
    printk( "Xilinx bp config  = 0x%x  \n", lptcPtr->bp_config );
    printk( "Xilinx bp status  = 0x%x  \n", lptcPtr->bp_status );

    regval = lptcPtr->status;
    if ( regval & LPTC_STATUS_OK )
        printk( "LPTC Status = OK\n" );
    else
        printk( "LPTC Status = BAD \n" );
    if ( regval & LPTC_STATUS_UPLINK_OK )
        printk( "LPTC Uplink = OK\n" );
    else
        printk( "LPTC Uplink = BAD \n" );
    printk( "LPTC Leap Seconds = %d\n",
            ( ( regval & LPTC_STATUS_LEAP_SEC ) >> 8 ) );
    printk( "LPTC Leap Seconds = 0x%x\n",
            ( ( regval & LPTC_STATUS_LEAP_SEC ) ) );

    lptcPtr->bp_config = LPTC_CMD_STOP_CLK_ALL;
    msleep( 10 );
    regval = lptcPtr->bp_status;
    if ( regval & LPTC_BPS_BP_PRESENT )
        printk( "LPTC backplane present = OK\n" );
    else
        printk( "LPTC BACKPLANE IS NOT PRESENT \n" );
    pCds->card_count[ LPTC ]++;
}

void
lptc_enable_all_slots( CDS_HARDWARE* pCds )
{
    unsigned int   regval;
    int            ii, jj;
    LPTC_REGISTER* lptcPtr;

    for ( jj = 0; jj < pCds->card_count[ LPTC ]; jj++ )
    {
        lptcPtr = (LPTC_REGISTER*)pCds->lptc[ jj ];
        for ( ii = 0; ii < LPTC_IOC_SLOTS; ii++ )
        {
            switch ( pCds->ioc_config[ ii ] )
            {
            case GSC_18AI64SSC:
            case GSC_18AI32SSC1M:
#ifdef SERVO512K
                lptcPtr->slot_info[ ii ].config =
                    ( LPTC_SCR_ADC_SET | IOC_CLK_FAST );
#else
                lptcPtr->slot_info[ ii ].config =
                    ( LPTC_SCR_ADC_SET | IOC_CLK_SLOW );
#endif
                break;
            case GSC_16AI64SSA:
                lptcPtr->slot_info[ ii ].config =
                    ( LPTC_SCR_ADC_SET | IOC_CLK_SLOW );
                break;
            case GSC_16AO16:
            case GSC_18AO8:
            case GSC_20AO8:
                lptcPtr->slot_info[ ii ].config =
                    ( LPTC_SCR_DAC_SET | IOC_CLK_SLOW );
                break;
            default:
                lptcPtr->slot_info[ ii ].config =
                    ( LPTC_SCR_ADC_SET | IOC_CLK_SLOW );
                break;
            }
            if ( ii == 0 )
                lptcPtr->slot_info[ ii ].config |= LPTC_SCR_ADC_DT_ENABLE;
            udelay( MAX_UDELAY );
            regval = lptcPtr->slot_info[ ii ].config;
        }
    }
    udelay( MAX_UDELAY );
    udelay( MAX_UDELAY );
}

int
lptc_get_gps_time( CDS_HARDWARE* pCds, unsigned int* sec, unsigned int* usec )
{
    volatile u64 regval;

    LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ 0 ];
    regval = lptcPtr->gps_time;
    *usec = (regval & 0xffffffff) * LPTC_USEC_CONVERT;
    *sec = (regval >> 32) & 0xffffffff;
    return 0;
}
unsigned int
lptc_get_gps_usec( CDS_HARDWARE* pCds )
{
    unsigned int timeval;
    volatile u64 regval;

    LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ 0 ];
    regval = lptcPtr->gps_time;
    timeval = (regval & 0xffffffff) * LPTC_USEC_CONVERT;
    return timeval;
}
unsigned int
lptc_get_gps_sec( CDS_HARDWARE* pCds )
{
    volatile u64 regval;
    unsigned int sec;

    LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ 0 ];
    regval = lptcPtr->gps_time;
    sec = (regval >> 32) & 0xffffffff;
    return sec;
}

int
lptc_get_slot_config( CDS_HARDWARE* pCds, int slot )
{
    LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ 0 ];
    return ( lptcPtr->slot_info[ slot ].config & 0xffffff );
}

int
lptc_get_slot_status( CDS_HARDWARE* pCds, int slot )
{
    LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ 0 ];
    return ( lptcPtr->slot_info[ slot ].status & 0xffffff );
}

int
lptc_start_clock( CDS_HARDWARE* pCds )
{
    int            ii, jj;
    LPTC_REGISTER* lptcPtr;

    for ( jj = 0; jj < pCds->card_count[ LPTC ]; jj++ )
    {
        lptcPtr = (LPTC_REGISTER*)pCds->lptc[ jj ];
        lptcPtr->bp_config = LPTC_CMD_START_CLK_ALL;
        // Update lptc status to epics
        for ( ii = 0; ii < LPTC_IOC_SLOTS; ii++ )
            pLocalEpics->epicsOutput.lptMon[ ii ] =
                lptc_get_slot_status( &cdsPciModules, ii );
        for ( ii = 0; ii < LPTC_IOC_SLOTS; ii++ )
            pLocalEpics->epicsOutput.lptMon[ ( ii + LPTC_IOC_SLOTS ) ] =
                lptc_get_slot_config( &cdsPciModules, ii );
        pLocalEpics->epicsOutput.lpt_bp_config =
            lptc_get_bp_config( &cdsPciModules );
        pLocalEpics->epicsOutput.lpt_bp_status =
            lptc_get_bp_status( &cdsPciModules );
        pLocalEpics->epicsOutput.lpt_status =
            lptc_get_lptc_status( &cdsPciModules );
    }

    return lptcPtr->bp_status;
}

void
lptc_status_update( CDS_HARDWARE* pCds )
{
    int ii;

    LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ 0 ];
    // Update lptc status to epics
    for ( ii = 0; ii < LPTC_IOC_SLOTS; ii++ )
        pLocalEpics->epicsOutput.lptMon[ ii ] =
            lptc_get_slot_status( pCds, ii );
    for ( ii = 0; ii < LPTC_IOC_SLOTS; ii++ )
        pLocalEpics->epicsOutput.lptMon[ ( ii + LPTC_IOC_SLOTS ) ] =
            lptc_get_slot_config( &cdsPciModules, ii );
    pLocalEpics->epicsOutput.lpt_bp_config =
        lptc_get_bp_config( &cdsPciModules );
    pLocalEpics->epicsOutput.lpt_bp_status =
        lptc_get_bp_status( &cdsPciModules );
    pLocalEpics->epicsOutput.lpt_status =
        lptc_get_lptc_status( &cdsPciModules );
}

int
lptc_stop_clock( CDS_HARDWARE* pCds )
{
    int            jj;
    LPTC_REGISTER* lptcPtr;

    for ( jj = 0; jj < pCds->card_count[ LPTC ]; jj++ )
    {
        lptcPtr = (LPTC_REGISTER*)pCds->lptc[ jj ];
        lptcPtr->bp_config = LPTC_CMD_STOP_CLK_ALL;
        udelay( MAX_UDELAY );
        udelay( MAX_UDELAY );
    }
    return lptcPtr->bp_status;
}

int
lptc_get_lptc_status( CDS_HARDWARE* pCds )
{
    LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ 0 ];
    return lptcPtr->status;
}

int
lptc_get_bp_status( CDS_HARDWARE* pCds )
{
    LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ 0 ];
    return lptcPtr->bp_status;
}

int
lptc_get_bp_config( CDS_HARDWARE* pCds )
{
    LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ 0 ];
    return lptcPtr->bp_config;
}

void
lptc_dac_duotone( CDS_HARDWARE* pCds, int setting )
{
    LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ 0 ];
    if ( setting )
    {
        lptcPtr->slot_info[ 0 ].config |= LPTC_SCR_DAC_DT_ENABLE;
        lptcPtr->slot_info[ 1 ].config |= LPTC_SCR_DAC_DT_ENABLE;
    }
    else
    {
        lptcPtr->slot_info[ 0 ].config &= ~( LPTC_SCR_DAC_DT_ENABLE );
        lptcPtr->slot_info[ 1 ].config &= ~( LPTC_SCR_DAC_DT_ENABLE );
    }
}

void
lptc_slot_clk_set( CDS_HARDWARE* pCds, int slot, int enable )
{
    int jj;

    for ( jj = 0; jj < pCds->card_count[ LPTC ]; jj++ )
    {
        LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ 0 ];
        if ( enable )
            lptcPtr->slot_info[ slot ].config |= LPTC_SCR_CLK_ENABLE;
        else
            lptcPtr->slot_info[ slot ].config &= ~( LPTC_SCR_CLK_ENABLE );
    }
}

void
lptc_slot_clk_disable_all( CDS_HARDWARE* pCds )
{
    int slot = 0;
    int jj;

    for ( jj = 0; jj < pCds->card_count[ LPTC ]; jj++ )
    {
        LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ jj ];
        for ( slot = 0; slot < LPTC_IOC_SLOTS; slot++ )
            lptcPtr->slot_info[ slot ].config &= ~( LPTC_SCR_CLK_ENABLE );
    }
}

void
lptc_slot_clk_enable_all( CDS_HARDWARE* pCds )
{
    int slot = 0;
    int jj;

    for ( jj = 0; jj < pCds->card_count[ LPTC ]; jj++ )
    {
        LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ jj ];
        for ( slot = 0; slot < LPTC_IOC_SLOTS; slot++ )
            lptcPtr->slot_info[ slot ].config |= LPTC_SCR_CLK_ENABLE;
    }
}
