from .front_end import Process


class CDSRFMProcesses(object):
    def __init__(self, target_dir):
        self.target_dir = target_dir

    @staticmethod
    def epics():
        return Process("rts-cdsrfm-epics.service", "rts-cdsrfm-epics.service")

    @staticmethod
    def module():
        return Process("rts-cdsrfm-module.service", "rts-cdsrfm-module.service")
