#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include "drv/gpsclock.h"

// GPS offset from system time (depends on number of elapsed leap seconds)
static long gpsoffset;

static int initdone = 0;

int gpsclock_init(void) {
    int status;
    status = gpsclock_offset(&gpsoffset);
    initdone = 1;
    return status;
}

int gpsclock_offset(long *offset) {
    // TAI-UTC offset in seconds as of 1972
    const long INITIAL_TAI_OFFSET = 10;
    // GPS-TAI offset in seconds
    const long GPS_TAI_OFFSET = -19;

    char *savetz;
    time_t now, gpszero, posixutc, rightutc;
    struct tm now_tm, gpszero_tm = {.tm_year=80, .tm_mon=0, .tm_mday=6, .tm_hour=0, .tm_min=0, .tm_sec=0, .tm_wday=-1, .tm_yday=-1, .tm_isdst=-1};

    // save TZ
    savetz = getenv("TZ");
    if (savetz) {
        setenv("GPSCLOCK_TZ_ORIG", savetz, 1);
    }
    // get current time in posix/UTC (no leap seconds)
    setenv("TZ", "posix/UTC", 1);
    tzset();
    now = time(0);
    localtime_r(&now, &now_tm);
    posixutc = mktime(&now_tm);
    // get UTC time at GPS zero
    gpszero = mktime(&gpszero_tm);
    // get current time in right/UTC (includes leap seconds)
    setenv("TZ", "right/UTC", 1);
    tzset();
    rightutc = mktime(&now_tm);
    // restore TZ
    savetz = getenv("GPSCLOCK_TZ_ORIG");
    if (savetz) {
        setenv("TZ", savetz, 1);
        unsetenv("GPSCLOCK_TZ_ORIG");
    } else {
        unsetenv("TZ");
    }
    tzset();
    *offset = rightutc - posixutc;
    if (*offset == 0) {
        fprintf(stderr, "gpsclock: failed to set correct GPS time offset\n");
        return -1;
    }
    // add fixed offsets (TAI-UTC as of 1972 and GPS-TAI)
    *offset += INITIAL_TAI_OFFSET + GPS_TAI_OFFSET;
    // subtract gps zero
    *offset -= gpszero;

    return 0;
}

char *gpsclock_timestring(char *s, int size) {
    struct timespec clk;
    if (!initdone) { gpsclock_init(); }
    clock_gettime(CLOCK_REALTIME, &clk);
    snprintf(s, size, "%ld.%02ld\n", clk.tv_sec + gpsoffset, clk.tv_nsec/10000000L);
    return s;
}

void gpsclock_time(unsigned long *req) {
    struct timespec clk;
    if (!initdone) { gpsclock_init(); }
    clock_gettime(CLOCK_REALTIME, &clk);
    req[0] = clk.tv_sec + gpsoffset;
    req[1] = clk.tv_nsec / 1000;
    req[2] = clk.tv_nsec % 1000;
}
