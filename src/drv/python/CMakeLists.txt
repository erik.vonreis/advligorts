pybind11_add_module(ligo_shmem ligo_shmem.cc)
#target_include_directories(ligo_shmem PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/../../include ${CMAKE_CURRENT_SOURCE_DIR}/../mbuf)
target_link_libraries(ligo_shmem PRIVATE driver::shmem)
install(TARGETS ligo_shmem DESTINATION "${Python3_SITEARCH}")
