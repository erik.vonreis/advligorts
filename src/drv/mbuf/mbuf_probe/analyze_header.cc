//
// Created by erik.vonreis on 4/29/21.
//

#include <iostream>

#include "analyze_header.hh"
#include "shmem_all.h"

using namespace std;

namespace analyze
{
    MBufStructures analyze_header(volatile void * header_void)
    {
        auto header = reinterpret_cast<volatile SHMEM_HEADER *>(header_void);

        auto version = SHMEM_GET_VERSION(header);
        auto id = SHMEM_GET_ID(header);

        MBufStructures detected_type = MBUF_INVALID;

        if(id)
        {
           switch(id)
           {
           case AWG_DATA_ID:
               detected_type = MBUF_AWG_DATA;
               cout << "Detected AWG_DATA struct ";
               break;
           case TESTPOINT_CFG_ID:
               detected_type = MBUF_TP_CFG;
               cout << "Detected TESTPOINT_CFG struct ";
           }
        }
        else
        {
            //couldn't identify
            cout << "Could not identify structure\n";
        }

        if(version)
        {
            cout << "version " << version << endl;
        }
        else
        {
            if(id)
            {
                cout << "version 0 (illegal.  version should be positive) \n";
            }
            else
            {
                //ignore bad version if id was also bad.
            }
        }
        return detected_type;
    }

}