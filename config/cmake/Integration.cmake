# Functions to help use the integration test python code

function(integration_test)
    cmake_parse_arguments( "ARG"
            ""
            "NAME;SCRIPT"
            ""
            ${ARGN})

    message("integration_test requested")
    message("    NAME=${ARG_NAME}")
    message("    SCRIPT=${ARG_SCRIPT}")
    message("    DEPENDS=${ARG_DEPENDS}")
    message("SRC=${CMAKE_CURRENT_SOURCE_DIR}")
    message("BIN=${CMAKE_CURRENT_BINARY_DIR}")
    message("PRJ=${CMAKE_SOURCE_DIR}")

    add_test(NAME "${ARG_NAME}"
            COMMAND ${PYTHON_EXECUTABLE} -B "${CMAKE_CURRENT_SOURCE_DIR}/${ARG_SCRIPT}"
            WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}")
    set_property(TEST "${ARG_NAME}"
            PROPERTY ENVIRONMENT "PYTHONPATH=${CMAKE_SOURCE_DIR}/test/python_modules")
endfunction()